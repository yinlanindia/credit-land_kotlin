package com.app.island.cash.data.sigle

class CodeListBack {
    var list: List<Bean>? = null

    class Bean {
        var id = 0
        var name: String? = null
        var parentId = 0
        var address: String? = null
        var ifsc: String? = null
        var grade = 0
    }
}