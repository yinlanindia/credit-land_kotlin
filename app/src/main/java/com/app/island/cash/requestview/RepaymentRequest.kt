package com.app.island.cash.requestview

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.island.cash.acts.TabActivity
import com.app.island.cash.adapter.RepayAdapter
import com.app.island.cash.databinding.RepaymentFragBinding
import com.app.island.cash.data.sigle.RepayListCall
import com.app.island.cash.data.sigle.RepayListBack
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil

class RepaymentRequest {
    private var context: Activity
    private var dataBinding: RepaymentFragBinding
    private var adapter: RepayAdapter
    constructor(conxt:FragmentActivity,binding: RepaymentFragBinding){
        context = conxt
        dataBinding = binding
        adapter = RepayAdapter(context)
        dataBinding.swipe.setOnRefreshListener {
            getRepayList()
        }
        dataBinding.swipeTarget.layoutManager = LinearLayoutManager(context)
        dataBinding.swipeTarget.adapter = adapter
        getRepayList()
        dataBinding.btnSubmit.setOnClickListener {
            val intent = Intent(context, TabActivity::class.java)
            intent.putExtra("pos", 0)
            context.startActivity(intent)
        }
    }

    fun getRepayList(){
        val repayListCall = RepayListCall()
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).getRepayRecords(repayListCall).enqueue(object : RequestCallBack<HttpResult<RepayListBack>>() {
            override fun onSuccess(body: HttpResult<RepayListBack>?) {
                dataBinding.swipe.isRefreshing = false
                val list = body?.data?.list
                if (list.isNullOrEmpty()) {
                    dataBinding.empty.visibility = View.VISIBLE
                    dataBinding.have.visibility = View.GONE
                } else {
                    dataBinding.empty.visibility = View.GONE
                    dataBinding.have.visibility = View.VISIBLE
                    adapter.setListData(list)
                }
            }

            override fun onError(code: Int, msg: String) {
                super.onError(code, msg)
                dataBinding.swipe.isRefreshing=false
            }
        })
    }
}