package com.app.island.cash.requestview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.acts.EnterPhone
import com.app.island.cash.acts.EnterRegister
import com.app.island.cash.databinding.EnterStartActBinding
import com.app.island.cash.data.sigle.PhoneReq
import com.app.island.cash.data.sigle.IsPhoneExistsRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.SystemUtils
import com.app.island.cash.utilstools.ToastUtil

class EnterRequest {
    private var context:AppCompatActivity
    private var binding:EnterStartActBinding
    constructor(context:AppCompatActivity,binding: EnterStartActBinding){
        this.context = context
        this.binding = binding
        setOnClicck()
    }

    private fun setOnClicck() {
        binding.btnCheck.setOnClickListener { checkHavePhone() }
    }

    private fun checkHavePhone() {
        val phone: String = binding.edPhoneNumber.getText().toString().trim()
        if (phone.length != 10) {
            ToastUtil.toast("please enter correct phone number")
            return
        }
        NetWorkLoadingUtil.showDialog(context)
        val exists = NetClient.getService(
            AppService::class.java
        ).isPhoneExists(PhoneReq(phone, SystemUtils.getIMEI(context)))
        exists.enqueue(object : RequestCallBack<HttpResult<IsPhoneExistsRec>?>() {
          override  fun onSuccess(body: HttpResult<IsPhoneExistsRec>?) {
              val intent = if (body?.data?.isExists == "10") {
                    Intent(context, EnterRegister::class.java)
                } else {
                    Intent(context, EnterPhone::class.java)
                }
                intent.putExtra("phone", phone)
                context.startActivity(intent)
            }
        })
    }


}