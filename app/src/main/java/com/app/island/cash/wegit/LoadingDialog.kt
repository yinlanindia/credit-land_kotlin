package com.app.island.cash.wegit

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.app.island.cash.R

/**
 * 自定义弹窗
 */
class LoadingDialog protected constructor(context: Context?, theme: Int, string: String?) : Dialog(
    context!!, theme
) {
    var tvLoadingTx: TextView
    var ivLoading: ImageView
    private val animation: Animation

    constructor(context: Context?) : this(context, R.style.loading_dialog, "Loading...") {}
    constructor(context: Context?, string: String?) : this(
        context,
        R.style.loading_dialog,
        string
    ) {
    }

    override fun show() {
        super.show()
        // 使用ImageView显示动画
        ivLoading.startAnimation(animation)
    }

    //关闭弹窗
    override fun dismiss() {
        super.dismiss()
        ivLoading.clearAnimation()
    }

    init {
        setCanceledOnTouchOutside(false) //点击其他区域时 true 关闭弹窗 false 不关闭弹窗
        setCancelable(false)
        setContentView(R.layout.layout_load) //加载布局
        tvLoadingTx = findViewById(R.id.tv_loading_tx)
        tvLoadingTx.text = string
        ivLoading = findViewById(R.id.iv_loading)
        // 加载动画
        animation = AnimationUtils.loadAnimation(
            context, R.anim.laoding
        )
        window!!.attributes.gravity = Gravity.CENTER //居中显示
        window!!.attributes.dimAmount = 0.5f //背景透明度 取值范围 0 ~ 1
    }
}