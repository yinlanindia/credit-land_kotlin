package com.app.island.cash.requestrisk.bean

class SenSorData {
    @JvmField
    var name: String? = null
    @JvmField
    var type: String? = null
    @JvmField
    var maxRange: String? = null
    @JvmField
    var minDelay: String? = null
    @JvmField
    var power: String? = null
    @JvmField
    var resolution: String? = null
    @JvmField
    var vendor: String? = null
    @JvmField
    var version: String? = null
}