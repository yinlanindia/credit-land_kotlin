package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.AccountMoreActivityBinding
import com.app.island.cash.requestview.AccountMoreReq

class AccountMore:BaseAct() {

    private lateinit var binding:AccountMoreActivityBinding
    private lateinit var accountMore: AccountMoreReq
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.account_more_activity)
        accountMore=AccountMoreReq(this,binding)
    }

    override fun onResume() {
        super.onResume()
        accountMore.huoquliebiao()
    }
}