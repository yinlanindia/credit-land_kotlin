package com.app.island.cash.data.list

data class Product(
    var id: Int? = 0,
    var merchantId: Int? = 0,
    var needCatch: Boolean? = false,
    var productAmount: String? = "",
    var productInterest: String? = "",
    var borrowId: String? = "",
    var productLogo: String? = "",
    var productName: String? = "",
    var productStatus: Int? = 0
)