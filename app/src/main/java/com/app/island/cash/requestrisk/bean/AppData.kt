package com.app.island.cash.requestrisk.bean

class AppData {
    //app名称
    @JvmField
    var appName: String? = null

    //app包名
    @JvmField
    var pkgName: String? = null

    //版本代码
    @JvmField
    var version: String? = null

    //app安装时间 毫秒时间戳（13位）
    @JvmField
    var installTime: Long = 0

    //app更新时间 毫秒时间戳（13位）
    @JvmField
    var updateTime: Long = 0
    @JvmField
    var isPreInstalled: String? = null
}