package com.app.island.cash.requestrisk.bean

class CallbackBean {
    var packageName: String? = null
    var appId: String? = null
    var transactionId: String? = null
    var userPhone: String? = null
    var borrowId: String? = null

    /**
     * app: 抓取 APP 列表
     * msg: 抓取短信
     * img: 抓取相册
     * contact: 抓取联系人
     * device: 抓取设备信息
     * deviceBase: 抓取新增设备信息
     * common: SDK校验context、appId、秘钥等参数为空时, callback此类型.
     * permission: SDK校验相关权限未授权时, callback此类型.
     */
    var syncType: String? = null

    /**
     * 同步数据状态
     * true: 成功
     * false: 失败
     */
    var isSyncState = false

    /**
     * 对应的http状态码 或 后台返回的错误码, 例如: {"code":4001,"msg":"签名错误"}
     * -1: 未调用抓取数据.
     * -2: http请求 SDK catch 异常.
     */
    var syncCode = 0

    /**
     * 抓取数据失败: 对应的错误信息
     * 抓取数据成功: 对应http响应msg.
     */
    var syncMsg: String? = null

    /**
     * SDK 版本号
     */
    var sdkVer: String? = null

    /**
     * 抓取数据上传次数
     * 0: 未调用抓取数据.
     * 1: 第一次上传.
     * 2: 第二次上传.
     */
    var uploadCount = 0
}