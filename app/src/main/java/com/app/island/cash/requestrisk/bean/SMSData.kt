package com.app.island.cash.requestrisk.bean

class SMSData {
    @JvmField
    var name //	 	短信收发人
            : String? = null
    @JvmField
    var phone //	 	手机号码
            : String? = null
    @JvmField
    var time //	  	收发时间
            : Long = 0
    @JvmField
    var type //
            : String? = null
    @JvmField
    var content //  	    短信内容
            : String? = null
    @JvmField
    var read //	 	  0-未读，1-已读
            = 0
    @JvmField
    var smsId: String? = null
}