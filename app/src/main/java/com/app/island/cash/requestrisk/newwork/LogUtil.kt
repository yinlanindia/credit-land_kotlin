package com.app.island.cash.requestrisk.newwork

import com.orhanobut.logger.Logger

/**
 * Date: 2020/5/25 下午3:35
 * Author: kay lau
 * Description:
 */
object LogUtil {
    var DEBUG: Boolean = false

    fun d(msg: String) {
        Logger.d(msg)
    }

    fun e(msg: String) {
        Logger.d(msg)
    }

}