package com.app.island.cash.utilstools

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.widget.Toast
import com.app.island.cash.Applications
import java.util.*

/**
 * Author: TinhoXu
 * E-mail: xth@duandai.com
 * Date: 2016/8/23 17:57
 *
 *
 * Description: Toast工具类
 */
object ToastUtil {
    /** 禁止重复提示  */
    private const val Interval = (3 * 1000).toLong()

    /** 消息软引用  */
    private val map = HashMap<String, Long>()

    /** Toast 对象  */
    private var toast: Toast? = null
    fun toast(msg: String?) {
        msg?.let {
            toast(Applications.mContext, it)
        }
    }

    fun toast(id: Int) {
        toast(Applications.mContext, Applications.mContext!!.getString(id))
    }

    private fun toast(context: Context?, msg: String) {
        if (!TextUtils.isEmpty(msg)) {
            var preTime: Long = 0
            if (map.containsKey(msg)) {
                preTime = map[msg]!!
            }
            val now = System.currentTimeMillis()
            if (now >= preTime + Interval) {
                if (toast != null) {
                    toast!!.cancel()
                }
                if (context != null) {
                    val toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                    toast.show()
                    map[msg] = now
                    ToastUtil.toast = toast
                }
            }
        }
    }
}