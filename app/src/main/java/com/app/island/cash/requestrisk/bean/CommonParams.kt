package com.app.island.cash.requestrisk.bean

class CommonParams {
    var appId: String? = null
    var packageName: String? = null
    var transactionId: String? = null
    var borrowId: String? = null
    var type: String? = null
    var userPhone: String? = null
    var version = "1.0.0"
    var deviceInfo: DeviceData? = null
    var contacts: List<ContactsData>? = null
    var installedApps: List<AppData>? = null
    var messageInfos: List<SMSData>? = null
    var imgInfoBeans: List<ImgData>? = null
}