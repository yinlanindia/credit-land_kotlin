package com.app.island.cash.utilstools

import android.app.Activity
import com.app.island.cash.wegit.LoadingDialog

object NetWorkLoadingUtil {
    private var dialog: LoadingDialog? = null
    fun showDialog(activity: Activity) {
        if (dialog == null) {
            dialog = LoadingDialog(activity)
        }
        try {
            if (!dialog!!.isShowing && !activity.isFinishing) {
                dialog!!.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissDialog() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog = null
        }
    }
}