package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import com.app.island.cash.R
import com.app.island.cash.databinding.ActivityTabBinding
import com.app.island.cash.fragments.ListFragment
import com.app.island.cash.fragments.RepaymentListFrag
import com.app.island.cash.fragments.UserFragment
import com.app.island.cash.utilstools.ActivityManager
import com.app.island.cash.utilstools.ToastUtil
import com.gyf.immersionbar.ImmersionBar

class TabActivity : BaseAct() {
    private lateinit var binding: ActivityTabBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityTabBinding>(
            TabActivity@ this,
            R.layout.activity_tab_
        )
        homeList = ListFragment()
        repayFrag = RepaymentListFrag()
        mineFrag = UserFragment()
        initListener()
    }

    private lateinit var homeList: ListFragment
    private lateinit var repayFrag: RepaymentListFrag
    private lateinit var mineFrag: UserFragment

    private fun initListener() {
        binding.llHome.setOnClickListener(::onClick)
        binding.llRepayment.setOnClickListener(::onClick)
        binding.llUser.setOnClickListener(::onClick)
        binding.llHome.performClick()
    }


    override fun onResume() {
        super.onResume()
        if (homeList.isVisible) {
            homeList.reqHomeData()
        }
        if (repayFrag.isVisible) {
            repayFrag.getRepayList()
        }
    }


    fun onClick(view: View) {
        val transaction = supportFragmentManager.beginTransaction()
        hiddenFrag(transaction)
        when (view.id) {
            R.id.ll_home -> {
                initTabsAndText(0)
                ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.white)
                    .navigationBarColor(R.color.black).statusBarDarkFont(true, 0.2f).init()
                if (!homeList.isAdded) {
                    transaction.add(R.id.grag_root, homeList, "Home")
                }
                transaction.show(homeList)
            }
            R.id.ll_repayment -> {
                ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.color_4071ff)
                    .navigationBarColor(R.color.black).statusBarDarkFont(false, 0.2f).init()
                initTabsAndText(1)
                if (!repayFrag.isAdded) {
                    transaction.add(R.id.grag_root, repayFrag, "Repayment")
                }
                transaction.show(repayFrag)
            }
            R.id.ll_user -> {
                initTabsAndText(2)
                ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.color_4071ff)
                    .navigationBarColor(R.color.black).statusBarDarkFont(false, 0.2f).init()
                if (!mineFrag.isAdded) {
                    transaction.add(R.id.grag_root, mineFrag, "Mine")
                }
                transaction.show(mineFrag)
            }
        }
        transaction.commit()
    }

    private fun hiddenFrag(transaction: FragmentTransaction) {
        transaction.hide(homeList)
        transaction.hide(repayFrag)
        transaction.hide(mineFrag)
    }

    private fun initTabsAndText(pos: Int) {
        binding.ivHome.isSelected = pos == 0
        binding.ivRepayment.isSelected = pos == 1
        binding.ivUser.isSelected = pos == 2
        binding.tvHome.isSelected = pos == 0
        binding.tvRepayment.isSelected = pos == 1
        binding.tvUser.isSelected = pos == 2
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        binding.llHome.performClick()
    }

    private var time: Long = 0
    override fun onBackPressed() {
        if (System.currentTimeMillis() - time > 2000) {
            time = System.currentTimeMillis()
            ToastUtil.toast("Press again to exit app")
        } else {
            ActivityManager.instance.finishAllActivity()
        }
    }
}