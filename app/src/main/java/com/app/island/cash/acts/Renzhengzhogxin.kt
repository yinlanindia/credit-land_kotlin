package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.RenzhengCenterActBinding
import com.app.island.cash.requestview.RenzhengReq

class Renzhengzhogxin:BaseAct() {
private lateinit var rezhengreq:RenzhengReq
    private lateinit var renzhengCenterActBinding: RenzhengCenterActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        renzhengCenterActBinding = DataBindingUtil.setContentView(this, R.layout.renzheng_center_act)
        rezhengreq = RenzhengReq(this,renzhengCenterActBinding)
    }

    override fun onResume() {
        super.onResume()
        rezhengreq.huoqurenzhengzhuangtai()
    }
}