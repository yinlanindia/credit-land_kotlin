package com.app.island.cash.data.sigle

class ContactListRec {
    var list: List<ListBean>? = null

    class ListBean {
        var id = 0
        var name: String? = null
        var phone: String? = null
        var userId = 0
        var relation: String? = null
        var type: String? = null
        var merchantId = 0
    }
}