package com.app.island.cash.acts

import android.os.Build
import android.os.Bundle
import android.webkit.WebSettings
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ActHtmlViewBinding

class HtmlView : BaseAct() {

    private lateinit var binding: ActHtmlViewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.act_html_view)
        val url = intent.getStringExtra("url")
        val title = intent.getStringExtra("title")
        binding.tvPageTitle.text=title
        binding.ivFinish.setOnClickListener { finish() }
        initIntentParams(url)
    }
    

     fun initIntentParams(url: String?) {
        val setting = binding.web.settings
        // 支持缩放
        setting.setSupportZoom(false)
        // 设置支持缩放 + -
        setting.builtInZoomControls = false
        // 关闭 webView 中缓存
        /**/setting.cacheMode = WebSettings.LOAD_NO_CACHE
        setting.useWideViewPort = true
        setting.loadWithOverviewMode = true
        // 设置WebView属性，能够执行Javascript脚本
        setting.javaScriptEnabled = true
        setting.savePassword = false
        setting.domStorageEnabled = true
        setting.defaultTextEncodingName = "utf-8"

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            dealJavascriptLeak(webView);
        }*/if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.web.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        binding.web.loadUrl(url!!)
    }

}