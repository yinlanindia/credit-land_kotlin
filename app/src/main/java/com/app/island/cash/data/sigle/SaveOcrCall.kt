package com.app.island.cash.data.sigle

import java.io.File

class SaveOcrCall {
    var panImg: File? = null
    var livingImg: File? = null
    var frontImg: File? = null
    var backImg: File? = null
    var realName: String? = null
    var idNo: String? = null
    var dateOfBirth: String? = null
    var gender: String? = null
    var pinCode: String? = null
    var idAddr: String? = null
    var panCode: String? = null
    var adBackJson: String? = null
    var adFrontJson: String? = null
    var panJson: String? = null
}