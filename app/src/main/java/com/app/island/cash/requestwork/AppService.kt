package com.app.island.cash.requestwork

import com.app.island.cash.data.list.HomeListBack
import com.app.island.cash.data.sigle.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface AppService {
    @POST(UrlData.indexMarket)
    fun findIndexMarket(): Call<HttpResult<HomeListBack>>

    @POST(UrlData.moreLoan)
    fun canApplyMore(): Call<HttpResult<Any>>

    //权限申请页面接口
    @GET(UrlData.urlList)
    fun cOmmonUrl(): Call<HttpResult<UrlListRec>>

    @GET(UrlData.userState)
    fun userAuthState(): Call<HttpResult<AuthStateRec>>

    @POST(UrlData.repayDetail)
    fun getRepayDetailInfo(@Body borrowIdCall: BorrowIdCall): Call<HttpResult<RepayDataBack>>

    @POST("/api/act/borrow/findAll.htm")
    fun getRepayRecords(@Body borrowIdCall: RepayListCall): Call<HttpResult<RepayListBack>>

    //所有订单
    @POST(UrlData.pageData)
    fun page(@Body borrowIdCall: RepayListCall): Call<HttpResult<RepayCordeListBack>>

    @GET(UrlData.findBorrow)
    fun getBorrowDetail(@Query("borrowId") borrowIdCall: String): Call<HttpResult<DetailBack>>

    @Multipart
    @POST(UrlData.imageSacn)
    fun imageScan(
        @HeaderMap head: Map<String, String>,
        @PartMap params: Map<String,@JvmSuppressWildcards RequestBody>
    ): Call<HttpResult<CardInfoBack>>

    @POST(UrlData.sdkStatus)
    fun getSdkReportStatusNew(@Body phoneReq: SDKSubmitCall): Call<HttpResult<Any>>

    @Multipart
    @POST(UrlData.nameSave)
    fun saveNameAuthSave(
        @HeaderMap head: Map<String, String>,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>
    ): Call<HttpResult<Any>>

    @Multipart
    @POST(UrlData.faceTure)
    fun faceTure(
        @HeaderMap head: Map<String, String>,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>
    ): Call<HttpResult<CardInfoBack>>

    @GET(UrlData.liveScore)
    fun getLivenessScore(@Query("livenessId") livenessId: String): Call<HttpResult<Any>>

    @GET(UrlData.authRealName)
    fun authRealNameInfo(): Call<HttpResult<UserCardRec>>

    @GET(UrlData.dicList)
    fun getDicList(@Query("type") type: String): Call<HttpResult<SelectListDataRec>>

    @POST(UrlData.infoSave)
    fun saveUInfo(@Body phoneReq: SaveUInfoCall): Call<HttpResult<Any>>

    @POST(UrlData.applyINfo)
    fun getProductionInfo(@Body phoneReq: ProductionReq): Call<HttpResult<ProductResp>>

    @POST(UrlData.applyLoan)
    fun submitApply(@Body phoneReq: AppluSubmitCall): Call<HttpResult<ApplySubmitBack>>

    @GET(UrlData.getUserInfo)
    fun userInfo(): Call<HttpResult<UserInfoRec>>

    @GET(UrlData.contactListData)
    fun contactInfo(): Call<HttpResult<ContactListRec>>

    /**
     * 保存联系人信息
     */
    @POST(UrlData.saveContact)
    fun saveContact(@Body sub: ContactDataCall): Call<HttpResult<Any>>

    @GET(UrlData.bankIfsc)
    fun bankBranch(@Query("parentId") parentId: Long): Call<HttpResult<CodeListBack>>

    /**
     * 获取银行卡信息
     */
    @POST(UrlData.saveBankInfo)
    fun saveBankInfo(@Body rec: SaveBankCall): Call<HttpResult<Any>>

    /**
     * 获取银行卡列表
     */
    @GET(UrlData.bankListData)
    fun cardList(): Call<HttpResult<BankListBack>>

    /**
     * 检查银行卡
     */
    @POST(UrlData.defaultData)
    fun setDefault(@Body bankAccountId: BankAccountId): Call<HttpResult<Any>>

    /**
     * 注册_验证手机号是否存在
     */
    @POST(UrlData.havePhone)
    fun isPhoneExists(@Body phoneReq: PhoneReq): Call<HttpResult<IsPhoneExistsRec>>

    /**
     * 注册_验证手机号是否存在
     */
    @POST(UrlData.login)
    fun login(@Body phoneReq: UserLoginCall): Call<HttpResult<UidTokenRec>>

    /**
     * 注册_验证手机号是否存在
     */
    @POST(UrlData.sendSms)
    fun sendSms(@Body phoneReq: OTPCall): Call<HttpResult<Any>>

    @POST(UrlData.register)
    fun register(@Body phoneReq: UserRegisterReq): Call<HttpResult<UidTokenRec>>

    //修改密码
    @POST(UrlData.changePwd)
    fun changeLoginPwd(@Body map: ChangePasswordCall): Call<HttpResult<Any>>

    @Multipart
    @POST(UrlData.feedback)
    fun updateFeedback(
        @HeaderMap head: Map<String, String>,
        @PartMap params: Map<String,@JvmSuppressWildcards RequestBody>
    ): Call<HttpResult<Any>>

    @POST(UrlData.forgetPwd)
    fun resetPwd(@Body sub: ResetPwdCall): Call<HttpResult<Any>>
}