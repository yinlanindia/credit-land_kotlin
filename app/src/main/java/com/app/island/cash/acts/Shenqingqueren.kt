package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ShenqingQuerenActBinding
import com.app.island.cash.requestview.ShenqingquerenReq

class Shenqingqueren:BaseAct() {
    private lateinit var binding:ShenqingQuerenActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.shenqing_queren_act)
        val name = intent.getStringExtra("name")
        binding.ivFinish.setOnClickListener { finish() }
        ShenqingquerenReq(this,binding,name!!)
    }
}