package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ActivityAppsuccBinding

class ApplySuccActivity : BaseAct() {

    private lateinit var binding: ActivityAppsuccBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_appsucc)
        initListener()
    }

     fun initListener() {
         binding.btnGoHome.setOnClickListener {
            startActivity(Intent(this@ApplySuccActivity, TabActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this@ApplySuccActivity, TabActivity::class.java))
        finish()
    }
}