package com.app.island.cash.requestview

import android.content.Intent
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.acts.SearchIfscActivity
import com.app.island.cash.databinding.AccountNewActBinding
import com.app.island.cash.data.sigle.SaveBankCall
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.ToastUtil

class AccountNewReq {
    private val context: AppCompatActivity
    private val binding: AccountNewActBinding

    constructor(context: AppCompatActivity, binding: AccountNewActBinding) {
        this.context = context
        this.binding = binding
        shezhijianting()
    }

    fun shezhijianting() {
        binding.tvIfc.setOnClickListener(View.OnClickListener {
            val intent = Intent(
                context,
                SearchIfscActivity::class.java
            )
            context.startActivityForResult(intent, 101)
        })
        binding.saveAccount.setOnClickListener { bancunzhanghu() }
    }

    private fun bancunzhanghu() {
        val code: String = binding.ceIfscCode.text.toString()
        val one: String = binding.accountOne.text.toString()
        val two: String = binding.accountConfirm.text.toString()
        if (TextUtils.isEmpty(code)) {
            ToastUtil.toast("please enter or select IFSC code")
            return
        }
        if (TextUtils.isEmpty(one)) {
            ToastUtil.toast("please enter your bank account number")
            return
        }
        if (TextUtils.isEmpty(two)) {
            ToastUtil.toast("please enter your bank account number again")
            return
        }

        if (one != two) {
            ToastUtil.toast("please enter same account number")
            return
        }
        val call = SaveBankCall(one, code.toUpperCase())
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).saveBankInfo(call).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                ToastUtil.toast(body?.msg)
                context.finish()
            }
        })
    }
}