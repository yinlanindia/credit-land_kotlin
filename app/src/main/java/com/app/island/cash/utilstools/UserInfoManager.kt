package com.app.island.cash.utilstools

import android.text.TextUtils
import com.tencent.mmkv.MMKV

class UserInfoManager {
    /**
     * 创建单例对象
     */
    private object UserInfoManagerInstance {
        var instance = UserInfoManager()
    }

    val isLogin: Boolean
        get() = !TextUtils.isEmpty(token) && !TextUtils.isEmpty(uid)
    var token: String
        get() = if (MMKV.defaultMMKV().decodeString("token") == null) "" else MMKV.defaultMMKV()
            .decodeString("token")!!
        set(token) {
            MMKV.defaultMMKV().encode("token", token)
        }
    var uid: String
        get() = if (MMKV.defaultMMKV().decodeString("uid") == null) "" else MMKV.defaultMMKV()
            .decodeString("uid")!!
        set(uid) {
            MMKV.defaultMMKV().encode("uid", uid)
        }
    val currentTIme: Long
        get() = MMKV.defaultMMKV().decodeLong("time")

    fun setCurrent() {
        MMKV.defaultMMKV().encode("time", System.currentTimeMillis())
    }

    val phoneNum: String
        get() = if (MMKV.defaultMMKV().decodeString("phone") == null) "" else MMKV.defaultMMKV()
            .decodeString("phone")!!

    fun setPhoneNumber(phone: String?) {
        MMKV.defaultMMKV().encode("phone", phone)
    }

    fun logout() {
        MMKV.defaultMMKV().remove("token")
        MMKV.defaultMMKV().remove("uid")
        MMKV.defaultMMKV().remove("phone")
    }

    companion object {
        /**
         * 调用单例对象
         */
        val instance: UserInfoManager
            get() = UserInfoManagerInstance.instance
    }
}