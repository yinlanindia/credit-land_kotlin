package com.app.island.cash.wegit

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Gravity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.app.island.cash.R

class CatchDialog(
    context: Context,
    private val downtime: Int,
    private val listener: DownFinishListener
) : Dialog(context, R.style.loading_dialog) {
    private var countDownTimer: CountDownTimer? = null
    private var animation: Animation? = null
    private lateinit var tvLoadingTx: TextView
    private lateinit var ivLoading: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_catch_load)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        tvLoadingTx = findViewById(R.id.tv_loading_tx)
        ivLoading = findViewById(R.id.iv_loading)
        // 加载动画
        animation = AnimationUtils.loadAnimation(
            context, R.anim.laoding
        )
        window!!.attributes.gravity = Gravity.CENTER //居中显示
        window!!.attributes.dimAmount = 0.5f //背景透明度 取值范围 0 ~ 1
        countDownTimer = object : CountDownTimer((downtime * 1000).toLong(), 1000) {
            override fun onTick(l: Long) {
                tvLoadingTx.setText("please wait " + l / 1000 + "s")
            }

            override fun onFinish() {
                listener.castchSucce()
                dismiss()
            }
        }
    }

    override fun show() {
        super.show()
        countDownTimer!!.start()
        ivLoading!!.startAnimation(animation)
    }

    override fun dismiss() {
        super.dismiss()
        ivLoading!!.clearAnimation()
        countDownTimer!!.cancel()
        countDownTimer = null
    }

    interface DownFinishListener {
        fun castchSucce()
    }
}