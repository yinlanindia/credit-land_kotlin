package com.app.island.cash.requestview

import android.content.Intent
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.R
import com.app.island.cash.acts.TabActivity
import com.app.island.cash.data.sigle.OTPCall
import com.app.island.cash.data.sigle.UidTokenRec
import com.app.island.cash.data.sigle.UrlListRec
import com.app.island.cash.data.sigle.UserRegisterReq
import com.app.island.cash.databinding.EnterRegisterActBinding
import com.app.island.cash.params.AppParams
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.SystemUtils
import com.app.island.cash.utilstools.ToastUtil
import com.app.island.cash.utilstools.UserInfoManager
import com.app.island.cash.wegit.UrlTextSpan
import retrofit2.Call

class EnterRegisterReq {
    private var context: AppCompatActivity
    private lateinit var registerSpan: UrlTextSpan
    private lateinit var privacySpan: UrlTextSpan
    private var binding: EnterRegisterActBinding
    private var phone: String

    constructor(context: AppCompatActivity, binding: EnterRegisterActBinding, phone: String) {
        this.context = context;
        this.binding = binding
        this.phone = phone
        setClcik()
        huoquxieyi()
    }

    private fun huoquxieyi() {
        NetClient.getService(
            AppService::class.java
        ).cOmmonUrl().enqueue(object : RequestCallBack<HttpResult<UrlListRec>?>() {
            override fun onSuccess(body: HttpResult<UrlListRec>?) {
                val list = body?.data?.list
                if (!list.isNullOrEmpty()) {
                    for (data in list) {
                        if ("h5_protocol_privacy" == data.code) {
                            privacySpan.setTitleUrl(
                                "Privacy Policy",
                                AppParams.net_url + data.value
                            )
                        }
                        if ("h5_protocol_register" == data.code) {
                            registerSpan.setTitleUrl("Terms of Use", AppParams.net_url + data.value)
                        }
                    }
                }
            }
        })
    }

    private fun setClcik() {
        binding.tvPageTitle.text = "Register"
        binding.ivFinish.setOnClickListener { context.finish() }
        val spannableString = SpannableString(context.getString(R.string.register_agree))
        registerSpan = UrlTextSpan(context)
        privacySpan = UrlTextSpan(context)
        spannableString.setSpan(registerSpan, 30, 45, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            privacySpan,
            46,
            spannableString.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        binding.tvRegister.movementMethod = LinkMovementMethod.getInstance()
        binding.tvRegister.text = spannableString
        binding.timeButton.setLength(60 * 1000)
        binding.timeButton.setOnClickListener { sendCode() }
        binding.btnSubmit.setOnClickListener { registerUser() }
    }

    private fun registerUser() {
        val otp: String = binding.edOtp.text.toString().trim()
        val password: String = binding.edPassword.text.toString().trim()
        if (!binding.agree.isChecked) {
            ToastUtil.toast("please read and agree privacy policy first")
            return
        }
        if (TextUtils.isEmpty(otp)) {
            ToastUtil.toast("please enter otp")
            return
        }

        if (TextUtils.isEmpty(password)) {
            ToastUtil.toast("please enter password")
            return
        }

        if (password.length < 6 || password.length > 16) {
            ToastUtil.toast("The password length is between 6 and 16 bits")
            return
        }
        val registerReq = UserRegisterReq()
        registerReq.loginName = phone //String 是 - - 注册⼿机号

        registerReq.loginPwd = SystemUtils.md5(password) //String 是 - - 注册密码的MD5值

        registerReq.vcode = otp //String 是 - - 验证码

        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).register(registerReq).enqueue(object : RequestCallBack<HttpResult<UidTokenRec>?>() {
            override fun onSuccess(body: HttpResult<UidTokenRec>?) {
                UserInfoManager.instance.setPhoneNumber(phone)
                UserInfoManager.instance.uid = body?.data?.userId!!
                UserInfoManager.instance.token = body?.data?.token!!
                context.startActivity(Intent(context, TabActivity::class.java))
                context.finish()
            }
        })
    }

    private fun sendCode() {
        NetWorkLoadingUtil.showDialog(context)
        val call = OTPCall(phone, "", "register")
        NetClient.getService(
            AppService::class.java
        ).sendSms(call).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                binding.timeButton.runTimer()
            }
        })
    }
}