package com.app.island.cash.data.sigle

class CardInfoBack {
    var birthday: String? = null
    var gender: String? = null
    var name: String? = null
    var idNumber: String? = null

    //back
    var other: String? = null
    var addressAll: String? = null
    var pin: String? = null
    var subdistrict: String? = null
    var district: String? = null
    var state: String? = null
    var fatherName: String? = null
}