package com.app.island.cash.requestview

import android.content.Intent
import android.provider.ContactsContract
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.databinding.RenzhengLianxirenActBinding
import com.app.island.cash.data.sigle.ContactDataCall
import com.app.island.cash.data.sigle.ContactListRec
import com.app.island.cash.data.sigle.SelectListDataRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.ToastUtil
import com.app.island.cash.wegit.ListSelectDialog

class Renzheng3Req {
    private val context: AppCompatActivity
    private val binding: RenzhengLianxirenActBinding
    private var xuanzeshuju: SelectListDataRec? = null
    private var zhifuId: Int = 0

    constructor(
        context: AppCompatActivity,
        binding: RenzhengLianxirenActBinding,
        renzhengwancheng: Boolean
    ) {
        this.context = context
        this.binding = binding
        if (!renzhengwancheng) {
            binding.member.setOnClickListener{
                xuanzeshuju?.let {
                    ListSelectDialog(
                        context,
                        it.kinsfolkRelationList,
                        zhifuId
                    )
                        .setOnGenderDialogListener (object :ListSelectDialog.OnListSelectListener{
                            override fun onCallBack(selectedIndex: Int, item: String?) {
                            zhifuId = selectedIndex
                            binding.member.text = it.kinsfolkRelationList[selectedIndex].value
                        }}).show()
                }
            }
            binding.hisPhone.setOnClickListener{
                val jumpIntent = Intent(Intent.ACTION_PICK)
                jumpIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                context.startActivityForResult(jumpIntent, 100)
            }
            binding.friPhone.setOnClickListener{
                val jumpIntent1 = Intent(Intent.ACTION_PICK)
                jumpIntent1.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                context.startActivityForResult(jumpIntent1, 101)
            }
            binding.saveContact.setOnClickListener{
                baocunxinxi()
            }
        }
    }

    private fun baocunxinxi() {
        val member: String = binding.member.text.toString()
        val hisName: String = binding.hisName.text.toString()
        val hisPhone: String = binding.hisPhone.text.toString()
        val friName: String = binding.friName.text.toString()
        val friPhone: String = binding.friPhone.text.toString()
        if (TextUtils.isEmpty(member)) {
            ToastUtil.toast("please select your family member")
            return
        }
        if (TextUtils.isEmpty(hisName)) {
            ToastUtil.toast("please enter his or her name")
            return
        }
        if (TextUtils.isEmpty(hisPhone)) {
            ToastUtil.toast("please select your family phone number")
            return
        }
        if (TextUtils.isEmpty(friName)) {
            ToastUtil.toast("please enter your friend's name")
            return
        }
        if (TextUtils.isEmpty(friPhone)) {
            ToastUtil.toast("please select your friend's phone number")
            return
        }
        val call = ContactDataCall(
            "$friName,$hisName",
            "$friPhone,$hisPhone", "friend,$member", "20,10"
        )
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).saveContact(call).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
           override fun onSuccess(body: HttpResult<Any>?) {
                ToastUtil.toast(body?.msg)
                context.finish()
            }
        })
    }

    fun getLiianxirenIInfo() {
        NetClient.getService(
            AppService::class.java
        ).contactInfo().enqueue(object : RequestCallBack<HttpResult<ContactListRec>?>() {
          override  fun onSuccess(body: HttpResult<ContactListRec>?) {
                val list = body?.data?.list
                if (!list.isNullOrEmpty()) {
                    for (bean in list) {
                        if ("10" == bean.type) {
                            binding.member.text = bean.relation
                            binding.hisName.setText(bean.name)
                            binding.hisPhone.text = bean.phone
                        } else {
                            binding.friName.setText(bean.name)
                            binding.friPhone.text = bean.phone
                        }
                    }
                }
            }
        })
    }

    fun getXuanzexinxi() {
        NetClient.getService(
            AppService::class.java
        ).getDicList("KINSFOLK_RELATION")
            .enqueue(object : RequestCallBack<HttpResult<SelectListDataRec>?>() {
                override fun onSuccess(body: HttpResult<SelectListDataRec>?) {
                    xuanzeshuju = body?.data
                }
            })
    }
}