package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.AboutUsActBinding
import com.app.island.cash.utilstools.SystemUtils

class AboutAct:BaseAct() {
    private lateinit var binding: AboutUsActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(AboutAct@this, R.layout.about_us_act)
        binding.tvPageTitle.text="About us"
        binding.tvVersion.text = "V" + SystemUtils.getVersionName(this)
        binding.ivFinish.setOnClickListener { finish() }
    }
}