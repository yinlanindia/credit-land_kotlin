package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.EnterPhoneActBinding
import com.app.island.cash.requestview.EnterPhoneRequest

class EnterPhone:BaseAct() {
private lateinit var bindng:EnterPhoneActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindng = DataBindingUtil.setContentView(this, R.layout.enter_phone_act)
        bindng.ivFinish.setOnClickListener { finish() }
        bindng.tvPageTitle.text = "Login"
        val phone = intent.getStringExtra("phone")
        bindng.edPhoneNumber.text = phone
        phone?.let {
            EnterPhoneRequest(this,bindng,it)
        }
    }
}