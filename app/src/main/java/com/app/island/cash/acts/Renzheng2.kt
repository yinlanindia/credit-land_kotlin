package com.app.island.cash.acts

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.RenzhengXinxiiActBinding
import com.app.island.cash.requestview.Renzheng2Req

class Renzheng2:BaseAct() {
    private lateinit var binding:RenzhengXinxiiActBinding
    private lateinit var renzheng:Renzheng2Req
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.renzheng_xinxii_act)
        binding.tvPageTitle.text="Personal Information"
        binding.ivFinish.setOnClickListener { finish() }
       val state = intent.getBooleanExtra("state", false)
        renzheng = Renzheng2Req(this,binding,state)
        if (state) {
            binding.saveUser.visibility = View.INVISIBLE
            renzheng.huoquxinxi()
        } else {
           val type =
                "EDUCATIONAL_STATE,MARITAL_STATE,ACCOMMODATION_TYPE,POSITION,LOAN_PURPOSE,SALARY_RANGE,CHILDREN_NUMBER,SALARY_TYPE"
            renzheng.huoquxuanzeshuju(type)
        }
    }

}