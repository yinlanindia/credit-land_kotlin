package com.app.island.cash.data.sigle

class AuthStateRec {
    var realNameAuthState: String? = null
    var contactState: String? = null
    var bankCardState: String? = null
    var personalInfoState: String? = null
}