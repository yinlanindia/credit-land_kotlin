package com.app.island.cash.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.app.island.cash.R
import com.app.island.cash.data.list.Product
import com.app.island.cash.databinding.ItemHomeBottomAdapterBinding
import com.app.island.cash.databinding.ItemHomeTopAdapterBinding
import java.util.*

class HomeAdapter_(private val context: Context, private val jumpListener: JumpListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val productData: MutableList<Product>
    fun setProductData(data: List<Product>?) {
        productData.clear()
        productData.addAll(data!!)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == productData.size) {
            0x1
        } else {
            0x2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0x1) {
            TopViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_home_top_adapter,
                    parent,
                    false
                )
            )
        } else {
            BottomViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_home_bottom_adapter,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == 0x2) {
            val data = productData[position]
            fillBottomData(holder as BottomViewHolder, data, position)
        } else {
        }
    }

    private fun fillBottomData(holder: BottomViewHolder, data: Product, position: Int) {
        Glide.with(context).load(data.productLogo).into(holder.binding.icProductLogo)
        holder.binding.tvTitle.text = data.productName
        holder.binding.productName.text = data.productAmount
        if (position >= 1) {
            holder.binding.rlTag.visibility = View.GONE
        } else {
            holder.binding.rlTag.visibility = View.VISIBLE
        }
        //        产品状态 1、可申请 2、 申请已满 3、审核中 4、审核拒绝 5、不展示  6 审核通过 7 放款中  8 待还款 9 逾期
        if (data.productStatus == 1) {
            holder.binding.btnApply.text = "Apply"
        } else if (data.productStatus == 10) {
            holder.binding.btnApply.text = "Apply Again"
        } else if (data.productStatus == 2 || data.productStatus == 4) {
            holder.binding.btnApply.text = if (data.productStatus == 2) "Full" else "Rejected"
        } else if (data.productStatus == 3 || data.productStatus == 6 || data.productStatus == 7) {
            holder.binding.btnApply.text =
                if (data.productStatus == 3) "Reviewing" else if (data.productStatus == 6) "Approved" else "Disbursing"
        } else {
            holder.binding.btnApply.text = "Repay Now"
        }
        holder.binding.btnApply.setOnClickListener(View.OnClickListener {
            if (data.productStatus == 2) return@OnClickListener
            jumpListener.jumpActivity(data)
        })
    }

    override fun getItemCount(): Int {
        return productData.size + 1
    }

    interface JumpListener {
        fun jumpActivity(productListBean: Product)
    }

    inner class BottomViewHolder(val binding: ItemHomeBottomAdapterBinding) :
        RecyclerView.ViewHolder(
            binding.root
        )

    inner class TopViewHolder(private val binding: ItemHomeTopAdapterBinding) :
        RecyclerView.ViewHolder(
            binding.root
        )

    init {
        productData = ArrayList()
    }
}