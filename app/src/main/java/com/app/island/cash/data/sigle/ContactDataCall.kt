package com.app.island.cash.data.sigle

class ContactDataCall(
    /**
     * 姓名
     */
    private val name: String,
    /**
     * 电话号码
     */
    private val phone: String,
    /**
     * 关系(中文)
     */
    private val relation: String,
    /**
     * 是否直系,10直系，20其他
     */
    private val type: String
)