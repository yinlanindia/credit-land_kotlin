package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.AccountNewActBinding
import com.app.island.cash.requestview.AccountNewReq

class AccountNew : BaseAct() {
    private lateinit var binding: AccountNewActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.account_new_act)
        binding.tvPageTitle.text=("Add Bank Account")
        binding.ivFinish.setOnClickListener { finish() }
        AccountNewReq(this,binding)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101 && resultCode == RESULT_OK) {
            val code = data!!.extras!!.getString("code")
            binding.ceIfscCode.setText(code)
        }
    }
}