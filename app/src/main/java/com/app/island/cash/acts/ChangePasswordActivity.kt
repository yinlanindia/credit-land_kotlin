package com.app.island.cash.acts

import android.os.Bundle
import android.text.TextUtils
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ActivityChangeBinding
import com.app.island.cash.data.sigle.ChangePasswordCall
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.ToastUtil
import com.app.island.cash.utilstools.UserInfoManager

class ChangePasswordActivity : BaseAct() {

    private lateinit var binding: ActivityChangeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_change)
        initIntentParams()
    }
    

     fun initIntentParams() {
        binding.tvPageTitle.text=("Change Password")
         binding.ivFinish.setOnClickListener { finish() }
        binding.tvPhoneNumber.text = UserInfoManager.instance.phoneNum
        binding.submit.setOnClickListener { changePwd() }
    }

    private fun changePwd() {
        val p1 = binding.newPwd.text.toString()
        val p2 = binding.pwdConfirm.text.toString()
        val p0 = binding.oldPwd.text.toString()
        if (TextUtils.isEmpty(p0)) {
            ToastUtil.toast("please enter original password")
            return
        }
        if (TextUtils.isEmpty(p1)) {
            ToastUtil.toast("please enter new password")
            return
        }
        if (TextUtils.isEmpty(p2)) {
            ToastUtil.toast("please confirm new password")
            return
        }
        if (p1 != p2) {
            ToastUtil.toast("please enter same password twice")
            return
        }
        val call = ChangePasswordCall(p1, p0)
        NetWorkLoadingUtil.showDialog(this)
        NetClient.getService(
            AppService::class.java
        ).changeLoginPwd(call).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                ToastUtil.toast(body?.msg)
                finish()
            }
        })
    }

}