package com.app.island.cash.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.app.island.cash.R
import com.app.island.cash.acts.OrderDetailActivity
import com.app.island.cash.databinding.AdapterRepayListBinding
import com.app.island.cash.data.sigle.RepayCordeListBack
import java.util.*

class LoanCordeAdapter(private val context: Context) :
    RecyclerView.Adapter<LoanCordeAdapter.ViewHolder>() {
    private val listData: MutableList<RepayCordeListBack.Bean>
    fun setListData(data: List<RepayCordeListBack.Bean>?) {
        listData.clear()
        listData.addAll(data!!)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(
                    context
                ), R.layout.adapter_repay_list, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = listData[position]
        Glide.with(context).load(data.productLogo).into(holder.binding.ivProductIcon)
        holder.binding.tvAppName.text = data.productName
        holder.binding.tvRange.text = data.createTimeStr
        holder.binding.tvInterest.text = "₹ " + data.amount
        //        10-审核中 20-自动审核成功  21自动审核不通过  22自动审核未决待人工复审
//            26人工复审通过 27人工复审不通过 29-放款中，31-放款失败 30-待还款 40-已还款 50已逾期 70取消贷款',
        if (data.state == "30" || "50" == data.state) {
            holder.binding.tvButton.text = "Repay Now"
        } else if (data.state == "40") {
            holder.binding.tvButton.text = "Repay Done"
        } else {
            holder.binding.tvButton.text = "Check"
        }
        holder.binding.tvButton.setOnClickListener {
            val intent = Intent(context, OrderDetailActivity::class.java)
            intent.putExtra("id", data.id)
            intent.putExtra("catch", false)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    inner class ViewHolder(val binding: AdapterRepayListBinding) : RecyclerView.ViewHolder(
        binding.root
    )

    init {
        listData = ArrayList()
    }
}