package com.app.island.cash.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.app.island.cash.R
import com.app.island.cash.acts.OrderDetailActivity
import com.app.island.cash.databinding.AdapterRepayListBinding
import com.app.island.cash.data.sigle.RepayListBack
import java.util.*

class RepayAdapter(private val context: Context) : RecyclerView.Adapter<RepayAdapter.ViewHolder>() {
    private val listData: MutableList<RepayListBack.Bean>
    fun setListData(data: List<RepayListBack.Bean>?) {
        listData.clear()
        listData.addAll(data!!)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(
                    context
                ), R.layout.adapter_repay_list, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = listData[position]
        Glide.with(context).load(data.productLogo).into(holder.binding.ivProductIcon)
        holder.binding.tvAppName.text = data.productName
        holder.binding.tvRange.text = data.applicationTimeStr
        holder.binding.tvInterest.text = data.repayment
        holder.binding.tvButton.setOnClickListener {
            val intent = Intent(context, OrderDetailActivity::class.java)
            intent.putExtra("id", data.borrowId)
            intent.putExtra("catch", false)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    inner class ViewHolder(val binding: AdapterRepayListBinding) : RecyclerView.ViewHolder(
        binding.root
    )

    init {
        listData = ArrayList()
    }
}