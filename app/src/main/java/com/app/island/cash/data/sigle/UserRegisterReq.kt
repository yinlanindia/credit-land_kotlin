package com.app.island.cash.data.sigle

import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.telephony.TelephonyManager
import android.text.format.Formatter
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerProperties
import com.app.island.cash.Applications
import com.app.island.cash.params.AppParams
import com.app.island.cash.utilstools.AddressUtils
import com.app.island.cash.utilstools.SystemUtils
import java.text.SimpleDateFormat
import java.util.*

class UserRegisterReq {
    var imei = SystemUtils.getIMEI(Applications.mContext!!) //String 是 - - ⼿机IMEI识别码
    var mac = SystemUtils.macAddress() //String 是 - - mac地址
    var ip = SystemUtils.iP //String 是 - - ip地址
    var operatingSystem = "android" //String 是 - - 固定值“android”
    var phoneBrand = Build.BRAND // String 是 - - ⼿机品牌
    var phoneMark = SystemUtils.getIMEI(Applications.mContext!!) //String 是 - - 设备标识，传⼿机的getDeviceId
    var phoneType = Build.MODEL //String 是 - - ⼿机型号 android.os.Build.MODEL
    var systemVersion = Build.VERSION.RELEASE //String 是 - - 系统版 本android.os.Build.VERSION.RELEASE
    var versionCode = SystemUtils.versionCode.toString() // String 是 - - 对应build中的 versionCode
    var versionName =
        SystemUtils.getVersionName(Applications.mContext!!) // String 是 - - 对应build⽂件中的versionName
    var registerAddr = AddressUtils.instance!!.detailAddress //String 是 - - 注册地址
    var registerCoordinate =
        AddressUtils.instance!!.latitude.toString() + "," + AddressUtils.instance!!.longitude //String 是 - - 注册经纬度，中间⽤,分割，例如“123.12,1212.12”
    var channelCode = AppParams.channel // String 是 - - 渠道id
    var androidId = SystemUtils.getAndroidID(Applications.mContext!!) //String 是 - - ⼿机androidId
    var defaultLanguage = Locale.getDefault().language //String 是 - - 默认语⾔
    var securityPatch = "" // String 是 - - 安全更新⽇期
    var sdkVersion = Build.VERSION.RELEASE //String 是 - - android.os.Build.VERSION.RELEASE
    var serial = Build.SERIAL // String 是 - - 设备序列号
    var rooted = false // String 是 - - 是否root
    var productionDate = productionDate() // String 是 - - 出⼚⽇期 yyyy-MM-dd格式
    var containSD = false //String 是 - - 是否有内置sd卡
    var ramCanUse = ramCanUse(Applications.mContext!!) //String 是 - - ram可⽤值
    var ramTotal = ramTotal(Applications.mContext!!) // String 是 - - ram总共
    var cashCanUse = cashCanUse(Applications.mContext!!) //String 是 - - 内存可⽤
    var cashTotal = cashTotal(Applications.mContext!!) // String 是 - - 内存总共
    var extraSD = false //String 是 - - 是否有外置SD卡
    var loginName //String 是 - - 注册⼿机号
            : String? = null
    var loginPwd //String 是 - - 注册密码的MD5值
            : String? = null
    var vcode //String 是 - - 验证码
            : String? = null
    var gpsAdId = AppsFlyerLib.getInstance()
        .getAppsFlyerUID(Applications.mContext!!) //String 是 - - adjust的gpsAdId
    var deviceWidth = getDeviceWidth(Applications.mContext!!).toString() //String 是 - - 分辨率屏幕宽
    var deviceHeight = getDeviceHeight(Applications.mContext!!).toString() //String 是 - - 分辨率⾼
    var telephony = getTelephonyName(Applications.mContext!!) //String 是 - - 运营商
    var appflyerCustomerUserId =
        AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID)

    //String
    fun getDeviceWidth(context: Context): Int {
        return context.resources.displayMetrics.widthPixels
    }

    /**
     * 获取运营商名字
     *
     * @param context context
     * @return int
     */
    fun getTelephonyName(context: Context): String {
        val telephonyManager =
            context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        // getSimOperatorName就可以直接获取到运营商的名字
        return telephonyManager.simOperatorName
    }

    /**
     * 获取设备高度（px）
     */
    fun getDeviceHeight(context: Context): Int {
        return context.resources.displayMetrics.heightPixels
    }

    fun cashTotal(context: Context?): String {
        val file = Environment.getDataDirectory()
        val statFs = StatFs(file.path)
        val blockSizeLong = statFs.blockSizeLong
        val blockCountLong = statFs.blockCountLong
        val size = blockCountLong * blockSizeLong
        return Formatter.formatFileSize(context, size)
    }

    fun cashCanUse(context: Context?): String {
        val file = Environment.getDataDirectory()
        val statFs = StatFs(file.path)
        val availableBlocksLong = statFs.availableBlocksLong
        val blockSizeLong = statFs.blockSizeLong
        return Formatter.formatFileSize(
            context, availableBlocksLong
                    * blockSizeLong
        )
    }

    /**
     * 获取 手机 可用 RAM
     */
    fun ramCanUse(context: Context): String {
        var size: Long = 0
        val activityManager = context
            .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val outInfo = ActivityManager.MemoryInfo()
        activityManager.getMemoryInfo(outInfo)
        size = outInfo.availMem
        return Formatter.formatFileSize(context, size)
    }

    fun productionDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val threadLocal = ThreadLocal<SimpleDateFormat>()
        threadLocal.set(sdf)
        return sdf.format(Date(Build.TIME))
    }

    companion object {
        fun ramTotal(context: Context): String {
            var size: Long = 0
            val activityManager = context
                .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val outInfo = ActivityManager.MemoryInfo()
            activityManager.getMemoryInfo(outInfo)
            size = outInfo.totalMem
            return Formatter.formatFileSize(context, size)
        }
    }
}