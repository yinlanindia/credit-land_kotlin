package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.island.cash.R
import com.app.island.cash.adapter.ProgressLOanAdapter
import com.app.island.cash.databinding.ActivityOrderSigleBinding
import com.app.island.cash.data.sigle.BorrowIdCall
import com.app.island.cash.data.sigle.DetailBack
import com.app.island.cash.data.sigle.RepayDataBack
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.params.AppParams
import com.app.island.cash.requestrisk.newwork.DataManager
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.UserInfoManager
import com.app.island.cash.wegit.ExtandDataDialog

class OrderDetailActivity : BaseAct() {
    private var borrowId: String? = null
    private var needCash = false
    private var progressLOanAdapter: ProgressLOanAdapter? = null
    private lateinit var binding: ActivityOrderSigleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_order_sigle)
        binding.tvPageTitle.text="Loan details"
        binding.ivFinish.setOnClickListener { finish() }
        initIntentParams()

    }

   fun initIntentParams() {
        borrowId = intent.getStringExtra("id")
        needCash = intent.getBooleanExtra("catch", false)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        progressLOanAdapter = ProgressLOanAdapter(this)
        binding.recyclerView.adapter = progressLOanAdapter
       initData()
    }

    fun initData() {
        NetWorkLoadingUtil.showDialog(this)
        val call = BorrowIdCall()
        call.borrowId = borrowId
        NetClient.getService(
            AppService::class.java
        ).getBorrowDetail(borrowId!!).enqueue(object : RequestCallBack<HttpResult<DetailBack>?>() {
            override fun onSuccess(body: HttpResult<DetailBack>?) {
                convertData(body?.data)
            }
        })
        if (needCash) {
            DataManager.instance.SynData(
                borrowId!!,
                UserInfoManager.instance.uid,
                UserInfoManager.instance.phoneNum,
                AppParams.sdk_url
            )
        }
    }

    private fun convertData(data: DetailBack?) {
        data?.let {
            binding.loanAmount.text = "₹ " + it.amount
            //        mDataBinding.recivedAmount.setText("₹ "+it.get());
            binding.productName.text = it.productName
            binding.applcationName.text = it.applicationDate
            binding.dueDate.text = it.repayTime
            binding.bankName.text = it.bankName
            binding.bankNo.text = it.cardNo
            binding.repaymentAmount.text = "₹ " + it.repayment
            progressLOanAdapter!!.setListBeans(it.list)
            if (TextUtils.isEmpty(it.actualRepayment) || TextUtils.isEmpty(it.actualRepayTime)) {
                binding.llAct.visibility = View.GONE
            } else {
                binding.llAct.visibility = View.VISIBLE
                binding.actRepayment.text = it.actualRepayment
                binding.actTime.text = it.actualRepayTime
            }
            binding.extendRepay.visibility = if (it.canExtension) View.VISIBLE else View.GONE
            binding.extendRepay.setOnClickListener {
                ExtandDataDialog(
                    this@OrderDetailActivity, data.extensionDate!!,
                    data.extensionAmount!!, data.extensionFee!!
                , View.OnClickListener { getRepayUrlData(true) }).show()
            }
            binding.repayButton.setOnClickListener { getRepayUrlData(false) }
            if (it.state == "30" || it.state == "50") {
                binding.repayButton.visibility = View.VISIBLE
            } else {
                binding.repayButton.visibility = View.GONE
            }
        }

    }

    private fun getRepayUrlData(extend: Boolean) {
        NetWorkLoadingUtil.showDialog(this)
        val call = BorrowIdCall()
        call.borrowId = borrowId
        call.isExtension = extend.toString()
        val repayDetailInfo = NetClient.getService(
            AppService::class.java
        ).getRepayDetailInfo(call)
        repayDetailInfo.enqueue(object : RequestCallBack<HttpResult<RepayDataBack>?>() {
            override fun onSuccess(body: HttpResult<RepayDataBack>?) {
                toRepayData(body?.data?.repayUrl!!)
            }
        })
    }

    private fun toRepayData(repayUrl: String) {
        if (!TextUtils.isEmpty(repayUrl)) {
            val intent = Intent(this, RepayWebviewActivity::class.java)
            intent.putExtra("url", repayUrl)
            startActivity(intent)
        }
    }
}