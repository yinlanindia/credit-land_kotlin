package com.app.island.cash.data.list

data class Auth(
    var qualified: Int? = 0,
    var result: Int? = 0,
    var total: Int? = 0
)