package com.app.island.cash.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.island.cash.R
import com.app.island.cash.databinding.RepaymentFragBinding
import com.app.island.cash.requestview.RepaymentRequest

class RepaymentListFrag:Fragment() {

    private lateinit var binding: RepaymentFragBinding
    private lateinit var request:RepaymentRequest
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.repayment_frag,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        request = RepaymentRequest(requireActivity(),binding)
    }

    fun getRepayList() {
        request.getRepayList()
    }


    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            getRepayList()
        }
    }
}