package com.app.island.cash.adapter

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.island.cash.R
import com.app.island.cash.acts.AccountNew
import com.app.island.cash.data.sigle.BankListBack
import java.util.*

class AddBankAdapter(private val context: Context, private val setBankListener: SetBankListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val list: MutableList<BankListBack.Bean>
    fun setNewData(data: List<BankListBack.Bean>?) {
        list.clear()
        list.addAll(data!!)
        notifyDataSetChanged()
    }

    val checkCard: BankListBack.Bean?
        get() {
            var bean: BankListBack.Bean? = null
            for (b in list) {
                if (TextUtils.equals("1", b.status)) bean = b
            }
            return bean
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            AddBank(
                LayoutInflater.from(context)
                    .inflate(R.layout.adapter_add_bank_layout, parent, false)
            )
        } else {
            BankHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.adapter_select_bank_layout, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == 0) {
            holder.itemView.setOnClickListener {
                context.startActivity(
                    Intent(
                        context,
                        AccountNew::class.java
                    )
                )
            }
        } else {
            val bankHolder = holder as BankHolder
            val bean = list[position]
            bankHolder.tvName.text =
                bean.bankName + " ( ***** " + bean.account!!.substring(bean.account!!.length - 4) + " )"
            bankHolder.tvName.isSelected = "1" == bean.status
            bankHolder.ivCheck.visibility = if ("1" == bean.status) View.VISIBLE else View.GONE
            bankHolder.itemView.setOnClickListener { setBankListener.setBank(bean) }
        }
    }

    interface SetBankListener {
        fun setBank(item: BankListBack.Bean)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size) {
            0
        } else {
            1
        }
    }

    override fun getItemCount(): Int {
        return list.size + 1
    }

    internal inner class BankHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView
        val ivCheck: ImageView

        init {
            tvName = itemView.findViewById(R.id.bank_name)
            ivCheck = itemView.findViewById(R.id.iv_choose)
        }
    }

    internal inner class AddBank(itemView: View) : RecyclerView.ViewHolder(itemView)

    init {
        list = ArrayList()
    }
}