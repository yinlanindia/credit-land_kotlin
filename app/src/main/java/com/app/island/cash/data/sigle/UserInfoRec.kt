package com.app.island.cash.data.sigle

class UserInfoRec {
    var salary: String? = null
    var marital: String? = null
    var firstName: String? = null
    var middleName: String? = null
    var lastName: String? = null
    var email: String? = null
    var occupation: String? = null
    var salaryType: String? = null
    var accommodationType: String? = null
    var childrenNumber: String? = null
    var loanPurpose: String? = null
    var education: String? = null
}