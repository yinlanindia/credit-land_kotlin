package com.app.island.cash.requestwork

class HttpResult<T> {
    var code = 0
    var msg: String? = null
    var data: T? = null
        private set

    fun setData(data: T) {
        this.data = data
    }
}