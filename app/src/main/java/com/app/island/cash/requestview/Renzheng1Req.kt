package com.app.island.cash.requestview

import ai.advance.liveness.sdk.activity.LivenessActivity
import ai.advance.sdk.iqa.IQAActivity
import ai.advance.sdk.quality.lib.enums.CardType
import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.app.island.cash.databinding.RenzhengKanpainActBinding
import com.app.island.cash.data.sigle.CardInfoDataCall
import com.app.island.cash.data.sigle.UserCardRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream

class Renzheng1Req {
    private val context:AppCompatActivity
    private val binding:RenzhengKanpainActBinding
    private val state:Boolean
     var zhengming:String?=null
     var fanming:String?=null
     var shuikaming:String?=null
     var renlian:String?=null
    private val renzheng1HttpUtil:Renzheng1HttpUtil

    constructor(ctx:AppCompatActivity,binding:RenzhengKanpainActBinding,state:Boolean){
        context = ctx
        this.binding = binding
        this.state= state
        if (state) {
            huoqukapainInfo()
            binding.saveSubmit.visibility = View.GONE
        } else {
            binding.rlFront.setOnClickListener{
                val intent: Intent = Intent(context, IQAActivity::class.java)
                intent.putExtra("cardType", CardType.AADHAAR_FRONT) // 默认值为 AUTO
                context.startActivityForResult(intent, 1010)
            }
            binding.rlBack.setOnClickListener{
                val intent1: Intent = Intent(context, IQAActivity::class.java)
                intent1.putExtra("cardType", CardType.AADHAAR_BACK) // 默认值为 AUTO
                context.startActivityForResult(intent1, 1011)
            }
            binding.rlFace.setOnClickListener{
                val intent3: Intent = Intent(
                    context,
                    LivenessActivity::class.java
                )
                context.startActivityForResult(intent3, 1013)
            }
            binding.rlPan.setOnClickListener{
                val intent2: Intent = Intent(context, IQAActivity::class.java)
                intent2.putExtra("cardType", CardType.PAN) // 默认值为 AUTO
                ctx.startActivityForResult(intent2, 1012)
            }
            binding.saveSubmit.setOnClickListener{
                baocunkapianxinxi()
            }
            binding.saveSubmit.visibility = View.VISIBLE
        }
        renzheng1HttpUtil = Renzheng1HttpUtil(context, binding)
    }

    private fun baocunkapianxinxi() {
        renzheng1HttpUtil.chaxunshifouyizhi(renlian)
    }

    private fun huoqukapainInfo() {
        NetWorkLoadingUtil.showDialog(context)
        val realNameInfo = NetClient.getService(
            AppService::class.java
        ).authRealNameInfo()
        realNameInfo.enqueue(object : RequestCallBack<HttpResult<UserCardRec>?>() {
            override fun onSuccess(body: HttpResult<UserCardRec>?) {
                Glide.with(context).load(body?.data?.frontImg)
                    .into(binding.ivFront)
                Glide.with(context).load(body?.data?.backImg)
                    .into(binding.ivBack)
                Glide.with(context).load(body?.data?.panImg)
                    .into(binding.ivPan)
                Glide.with(context).load(body?.data?.livingImg)
                    .into(binding.ivFace)
                settinginfo(body)
            }
        })
    }

    private fun settinginfo(body: HttpResult<UserCardRec>?) {
        binding.name.text = body?.data?.realName
        binding.adNo.text = body?.data?.idNo
        binding.birth.text = body?.data?.dateOfBirth
        binding.gender.text = body?.data?.sex
        binding.pinCode.text = body?.data?.pinCode
        binding.address.text = body?.data?.idAddr
        binding.panNo.text = body?.data?.panCode
    }

    fun huoqukapianxinxi(requestCode: Int) {
        val cardInfoDataCall: CardInfoDataCall = when (requestCode) {
            1010 -> CardInfoDataCall("AADHAAR_FRONT", File(zhengming))
            1011 -> CardInfoDataCall("AADHAAR_BACK", File(fanming))
            else -> CardInfoDataCall("PAN_FRONT", File(shuikaming))
        }
        renzheng1HttpUtil.huoqukapianxinxi(requestCode,cardInfoDataCall)
    }

    fun yanzhengdefen(livenessId: String?, bitmap: Bitmap?) {
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).getLivenessScore(livenessId!!).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                renlian = huoqutupian(bitmap!!)
                Glide.with(context).load(renlian)
                    .into(binding.ivFace)
            }
        })
    }

    private fun huoqutupian(bitmap: Bitmap): String {
        val file: File
        try {
            file = File(context.filesDir, System.currentTimeMillis().toString() + ".jpg")
            val bos = BufferedOutputStream(FileOutputStream(file))
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
            return file.absolutePath
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
}