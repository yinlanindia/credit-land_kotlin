package com.app.island.cash.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.island.cash.R
import com.app.island.cash.databinding.UserFragBinding
import com.app.island.cash.requestview.UserRequest
import com.app.island.cash.utilstools.UserInfoManager

class UserFragment:Fragment() {

private lateinit var dataBinding: UserFragBinding
private lateinit var userRequest: UserRequest
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.user_frag,container,false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.userPhone.text = UserInfoManager.instance.phoneNum

        userRequest = UserRequest(requireActivity(),dataBinding)
    }
}