package com.app.island.cash.acts

import ai.advance.liveness.lib.GuardianLivenessDetectionSDK
import ai.advance.liveness.lib.LivenessResult
import ai.advance.liveness.lib.Market
import ai.advance.sdk.quality.lib.GuardianImageQualitySDK
import ai.advance.sdk.quality.lib.ImageQualityResult
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.RenzhengKanpainActBinding
import com.app.island.cash.requestview.Renzheng1Req
import com.app.island.cash.utilstools.ToastUtil
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream

class Renzheng1 : BaseAct() {

    private lateinit var binding: RenzhengKanpainActBinding
    private lateinit var renzheng1Req:Renzheng1Req
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.renzheng_kanpain_act)
        val state = intent.getBooleanExtra("state", false)
        binding.tvPageTitle.text = "Identity Certification"
        binding.ivFinish.setOnClickListener { finish() }
        if (!state) {
            GuardianImageQualitySDK.init(
                application,
                "95e07a4fc156a03b",
                "b60ae2d1d29b0752",
                Market.India
            )
            GuardianLivenessDetectionSDK.init(
                application,
                "95e07a4fc156a03b",
                "b60ae2d1d29b0752",
                Market.India
            )
        }
        renzheng1Req = Renzheng1Req(this,binding,state)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (ImageQualityResult.isSuccess()) {
                val bitmap = ImageQualityResult.getBitmap() ?: return
                if (requestCode == 1010) {
                    renzheng1Req.zhengming = huoqutupian(bitmap)
                } else if (requestCode == 1011) {
                    renzheng1Req.fanming = huoqutupian(bitmap)
                } else if (requestCode == 1012) {
                    renzheng1Req.shuikaming = huoqutupian(bitmap)
                }
                renzheng1Req.huoqukapianxinxi(requestCode)
            }
            if (requestCode == 1013) {
                if (LivenessResult.isSuccess()) { // 活体检测成功
                    val livenessId = LivenessResult.getLivenessId() // 本次活体id
                    val bitmap = LivenessResult.getLivenessBitmap() // 本次活体图片
                    renzheng1Req.yanzhengdefen(livenessId, bitmap)
                } else { // 活体检测失败
                    val errorMsg = LivenessResult.getErrorMsg() // 失败原因
                    ToastUtil.toast(errorMsg)
                }
            }
        }
    }

    private fun huoqutupian(bitmap: Bitmap): String {
        val file: File
        try {
            file = File(filesDir, System.currentTimeMillis().toString() + ".jpg")
            val bos = BufferedOutputStream(FileOutputStream(file))
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
            return file.absolutePath
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

}