package com.app.island.cash.data.sigle

class BankListBack {
    var list: List<Bean>? = null

    class Bean {
        var id: String? = null
        var bankName: String? = null
        var account: String? = null
        var status //status  1 选中  2未选中
                : String? = null
    }
}