package com.app.island.cash.acts

import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ActRepayViewBinding
import com.just.agentweb.AgentWeb

class RepayWebviewActivity :BaseAct() {
    private var url: String? = null
    private lateinit var binding: ActRepayViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.act_repay_view)
        initIntentParams()
    }
    private fun initIntentParams() {
        binding.tvPageTitle.text=("Repayment")
        binding.ivFinish.setOnClickListener { finish() }
        url = intent.getStringExtra("url")
        AgentWeb.with(this)
            .setAgentWebParent(
                binding.web,
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
            .useDefaultIndicator()
            .createAgentWeb()
            .ready()
            .go(url)
    }
}