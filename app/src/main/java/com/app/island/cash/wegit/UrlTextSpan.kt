package com.app.island.cash.wegit

import android.content.Context
import android.content.Intent
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.ClickableSpan
import android.view.View
import com.app.island.cash.R
import com.app.island.cash.acts.HtmlView

class UrlTextSpan : ClickableSpan {
    private var context: Context
    private var title: String? = null
    private var url: String? = null

    constructor(context: Context) {
        this.context = context
    }

    constructor(context: Context, title: String?, url: String?) {
        this.context = context
        this.title = title
        this.url = url
    }

    fun setTitleUrl(title: String?, url: String?) {
        this.title = title
        this.url = url
    }

    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = false
        ds.color = context.resources.getColor(R.color.color_26357b)
    }

    override fun onClick(widget: View) {
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(url)) return
        val intent = Intent(context, HtmlView::class.java)
        intent.putExtra("title", title)
        intent.putExtra("url", url)
        context.startActivity(intent)
    }
}