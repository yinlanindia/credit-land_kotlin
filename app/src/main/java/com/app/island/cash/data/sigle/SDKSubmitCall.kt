package com.app.island.cash.data.sigle

class SDKSubmitCall(
    var borrowId: String,
    var message: String,
    var code: String,
    var reportStatus: String,
    var type: String
)