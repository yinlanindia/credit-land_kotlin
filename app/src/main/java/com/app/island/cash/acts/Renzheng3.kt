package com.app.island.cash.acts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.RenzhengLianxirenActBinding
import com.app.island.cash.requestview.Renzheng3Req

class Renzheng3:BaseAct() {
    private lateinit var binding:RenzhengLianxirenActBinding
    private lateinit var renzheng3Req: Renzheng3Req
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.renzheng_lianxiren_act)
        binding.tvPageTitle.text="Reference Contacts"
        binding.ivFinish.setOnClickListener { finish() }
        val state = intent.getBooleanExtra("state", false)
        renzheng3Req = Renzheng3Req(this,binding,state)
        if (state) {
            binding.saveContact.visibility = View.INVISIBLE
            renzheng3Req.getLiianxirenIInfo()
        } else {
            binding.saveContact.visibility = View.VISIBLE
            renzheng3Req.getXuanzexinxi()
        }
    }

    private fun huoqushoujihao(data: Uri?): String? {
        var number = ""
        try {
            if (data != null) {
                val cursor = contentResolver
                    .query(
                        data,
                        arrayOf(
                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                        ),
                        null,
                        null,
                        null
                    )
                if (cursor!!.moveToFirst()) {
                    number =
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            .replace(" ", "").replace("-", "")
                    return number
                }
                cursor.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return number
        }
        return number
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                binding.hisPhone.text = huoqushoujihao(data!!.data)
            } else {
                binding.friPhone.text = huoqushoujihao(data!!.data)
            }
        }
    }
}