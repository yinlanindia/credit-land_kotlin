package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.island.cash.R
import com.app.island.cash.adapter.LoanCordeAdapter
import com.app.island.cash.databinding.ActivityRecordeLoanBinding
import com.app.island.cash.data.sigle.RepayListCall
import com.app.island.cash.data.sigle.RepayCordeListBack
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil

class RecordsLoansActivity : BaseAct() {
    private lateinit var binding: ActivityRecordeLoanBinding
    private var adapter: LoanCordeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_recorde_loan)
        binding.tvPageTitle.text="Loan Record"
        binding.ivFinish.setOnClickListener { finish() }
        initIntentParams()
        listData()
    }

     private fun initIntentParams() {
        adapter = LoanCordeAdapter(this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
        binding.btnSubmit.setOnClickListener {
            val intent = Intent(this@RecordsLoansActivity, TabActivity::class.java)
            intent.putExtra("pos", 0)
            startActivity(intent)
        }
         binding.swipeRoot.setOnRefreshListener { listData() }
    }

    private fun listData() {
            val repayListCall = RepayListCall()
            NetWorkLoadingUtil.showDialog(this)
            NetClient.getService(
                AppService::class.java
            ).page(repayListCall).enqueue(object : RequestCallBack<HttpResult<RepayCordeListBack>?>() {
                override fun onSuccess(body: HttpResult<RepayCordeListBack>?) {
                    binding.swipeRoot.isRefreshing = false
                    convertData(body?.data?.list)
                }
            })
        }

    private fun convertData(list: List<RepayCordeListBack.Bean>?) {
        if (list == null || list.isEmpty()) {
            binding.empty.visibility = View.VISIBLE
            binding.swipeRoot.visibility = View.GONE
        } else {
            binding.swipeRoot.visibility = View.VISIBLE
            binding.empty.visibility = View.GONE
            adapter!!.setListData(list)
        }
    }

}