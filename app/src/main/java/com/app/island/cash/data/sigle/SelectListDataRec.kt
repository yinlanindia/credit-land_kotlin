package com.app.island.cash.data.sigle

import java.util.*

class SelectListDataRec {
    var positionList: List<ListItem> = ArrayList()
    var childrenNumberList: List<ListItem> = ArrayList()
    var salaryRangeList: List<ListItem> = ArrayList()
    var loanPurposeList: List<ListItem> = ArrayList()
    var accommodationTypeList: List<ListItem> = ArrayList()
    var salaryTypeList: List<ListItem> = ArrayList()
    var maritalStateList: List<ListItem> = ArrayList()
    var educationalStateList: List<ListItem> = ArrayList()
    var kinsfolkRelationList: List<ListItem> = ArrayList()

    inner class ListItem {
        var id: String? = null
        var value: String? = null
    }
}