package com.app.island.cash.data.sigle

class UrlListRec {
    var list: List<ListBean>? = null

    class ListBean {
        var name: String? = null
        var code: String? = null
        var value: String? = null
    }
}