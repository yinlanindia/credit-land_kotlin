package com.app.island.cash.data.sigle

class ResetPwdCall {
    var phone: String? = null
    var newPwd: String? = null
    var confirmPassword: String? = null
    var signMsg: String? = null
    var vcode: String? = null
}