package com.app.island.cash.acts

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.island.cash.R
import com.app.island.cash.adapter.CodeAdapter
import com.app.island.cash.databinding.ActivityIfscCodeBinding
import com.app.island.cash.data.sigle.CodeListBack
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil

class SearchIfscActivity : AppCompatActivity() {
    private lateinit var binding: ActivityIfscCodeBinding
    private var ifscCode: String? = null
    private var adapter: CodeAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFinishOnTouchOutside(false)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        val m = windowManager
        val d = m.defaultDisplay //为获取屏幕宽、高
        val p = window.attributes //获取对话框当前的参数值
        p.height = (d.height * 0.8).toInt() //高度设置为屏幕的0.6
        p.width = d.width
        p.alpha = 1.0f //设置本身透明度
        p.dimAmount = 0.5f //设置窗口外黑暗度
        window.attributes = p
        window.setGravity(Gravity.BOTTOM)
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ifsc_code)
        getListData(0)
        setListener()
        binding.swipeTarget.layoutManager = LinearLayoutManager(this)
        adapter = CodeAdapter(this)
        binding.swipeTarget.adapter = adapter
        adapter!!.setOnItemClickListener(object :CodeAdapter.OnItemClickListener{
            override fun OnItemClick(view: View?, item: CodeListBack.Bean, position: Int) {
                if (item.grade == 1) {
                    binding.tvTitle.text = "Select State"
                    binding.tvOne.text = item.name
                    binding.tvOne.visibility = View.VISIBLE
                } else if (item.grade == 2) {
                    binding.tvTitle.text = "Select City"
                    binding.tvTow.text = item.name
                    binding.tvTow.visibility = View.VISIBLE
                } else if (item.grade == 3) {
                    binding.tvTitle.text = "Select your IFSC"
                    binding.tvThree.text = item.name
                    binding.tvThree.visibility = View.VISIBLE
                } else {
                    ifscCode = item.ifsc
                    adapter!!.setSelected(position)
                    return
                }
                getListData(item.id.toLong())
            }

        })
    }

    private fun setListener() {
        binding!!.cancel.setOnClickListener { finish() }
        binding!!.confirm.setOnClickListener {
            if (!TextUtils.isEmpty(ifscCode)) {
                val intent = Intent()
                intent.putExtra("code", ifscCode)
                setResult(RESULT_OK, intent)
                finish()
            }
        }
    }

    private fun getListData(id: Long) {
        if (id != 0L) {
            NetWorkLoadingUtil.showDialog(this)
        }
        val httpResultCall = NetClient.getService(
            AppService::class.java
        ).bankBranch(id)
        httpResultCall.enqueue(object : RequestCallBack<HttpResult<CodeListBack>?>() {
            override fun onSuccess(body: HttpResult<CodeListBack>?) {
                adapter!!.setList(body?.data?.list)
            }
        })
    }
}