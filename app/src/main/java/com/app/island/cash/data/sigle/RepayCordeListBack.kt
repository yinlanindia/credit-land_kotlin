package com.app.island.cash.data.sigle

class RepayCordeListBack {
    var list: List<Bean>? = null

    class Bean {
        val id: String? = null
        val amount: Int? = null
        val realAmount: Double? = null
        val createTime: String? = null
        val productLogo: String? = null
        val state: String? = null
        val productName: String? = null
        val stateStr: String? = null
        val penaltyAmount: Int? = null
        val penaltyDay: Int? = null
        val repayAmount: Int? = null
        val createTimeStr: String? = null
    }
}