package com.app.island.cash.requestrisk.bean

class ContactsData {
    @JvmField
    var phone: String? = null
    @JvmField
    var name: String? = null
    @JvmField
    var updateTime: String? = null
    @JvmField
    var lastTimeContacted: String? = null
    @JvmField
    var timesContacted: String? = null
    @JvmField
    var source: String? = null
}