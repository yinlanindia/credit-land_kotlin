package com.app.island.cash.requestrisk.bean

class DeviceData {
    /**
     * 必填项
     */
    //内存可用
    @JvmField
    var cashCanUse: String? = null

    //内存总共
    @JvmField
    var cashTotal: String? = null

    //是否有内置sd卡
    @JvmField
    var containSd: String? = null

    //imei设备标识
    @JvmField
    var imei: String? = null

    //mac地址（手机）
    @JvmField
    var mac: String? = null

    //操作系统
    @JvmField
    var operatingSystem: String? = null

    //ram可用
    @JvmField
    var ramCanUse: String? = null

    //ram总共
    @JvmField
    var ramTotal: String? = null

    //是否root
    @JvmField
    var rooted: String? = null

    //系统版本
    @JvmField
    var systemVersions: String? = null

    //安卓id
    @JvmField
    var androidId: String? = null

    //是否有外置SD卡
    @JvmField
    var extraSd: String? = null

    // 出厂时间
    @JvmField
    var productionDate: String? = null

    /**
     * 选填项
     */
    //运营商的名称
    @JvmField
    var phoneBrand: String? = null

    //设备型号
    @JvmField
    var phoneType: String? = null

    //硬件设备序列号
    @JvmField
    var hardwareSerial: String? = null

    //应用build号
    @JvmField
    var buildId: String? = null

    //应用版本号
    @JvmField
    var buildName: String? = null

    //分辨率宽
    @JvmField
    var deviceWidth: String? = null

    //分辨率高
    @JvmField
    var deviceHeight: String? = null

    //设备语言
    @JvmField
    var defaultLanguage: String? = null

    //本机号码
    @JvmField
    var localPhone: String? = null

    //运营商
    @JvmField
    var carrier: String? = null

    //sdk版本
    @JvmField
    var sdkVersion: String? = null

    //蜂窝网络IP
    @JvmField
    var netIp: String? = null

    //wifi IP地址
    @JvmField
    var wifiIp: String? = null

    // 电池状态
    @JvmField
    var batteryStatus: String? = null

    //电池电量
    @JvmField
    var batteryLevel: String? = null

    //APP安装时间
    @JvmField
    var appInstallTime: Long = 0

    //设备名称
    @JvmField
    var deviceName: String? = null

    //gps经度
    @JvmField
    var gpsLongitude: String? = null

    //gps纬度
    @JvmField
    var gpsLatitude: String? = null

    //设备当前时间
    @JvmField
    var deviceTime: Long = 0

    //网络类型
    @JvmField
    var networkType: String? = null

    //是否使用vpn
    @JvmField
    var useVpn: String? = null

    //是否模拟器
    @JvmField
    var isSimulator: String? = null

    //google广告id
    @JvmField
    var gaid: String? = null
    @JvmField
    var ssid: String? = null
    @JvmField
    var wifiMac: String? = null
    @JvmField
    var linkSpeed = 0

    //3.1新增字段
    //设备最大电量
    @JvmField
    var batteryMax: String? = null

    // 手机主板名
    @JvmField
    var board: String? = null

    //CPU总数
    @JvmField
    var cpuNum = 0

    // 当前设备的手机号
    @JvmField
    var phone: String? = null

    //开机总时长
    @JvmField
    var openElapsedTime: Long = 0

    //开机总时长非休眠
    @JvmField
    var openUptime: Long = 0

    //用户当前时区
    @JvmField
    var timeZone: String? = null

    //3.2新增
    //============新增设备数据============
    //@ApiModelProperty("物理尺寸 （从这里开始及以下的字段是2.0版本新增的）")
    @JvmField
    var physicalSize: String? = null

    //@ApiModelProperty("内存卡大小")
    @JvmField
    var memoryCardSize: String? = null

    //@ApiModelProperty("内存卡可使用量")
    @JvmField
    var memoryCardUsableSize: String? = null

    //@ApiModelProperty("内存卡已使用量")
    @JvmField
    var memoryCardUsedSize: String? = null

    //@ApiModelProperty("语言环境的三字母缩写")
    @JvmField
    var localsLanguageAbbreviation: String? = null

    //@ApiModelProperty("此用户显示的语言环境语⾔的名称")
    @JvmField
    var localsLanguageName: String? = null

    //ApiModelProperty("此地区的国家/地区的缩写")
    @JvmField
    var countryOrAreaAbbreviation: String? = null

    //@ApiModelProperty("时区的ID")
    @JvmField
    var timeZoneId: String? = null

    //@ApiModelProperty("是否使用代理 0:否，1：是")
    @JvmField
    var whetherUseProxy: String? = null

    //@ApiModelProperty("是否开启 debug 调试 0:否，1：是")
    @JvmField
    var whetherEnableDebugMode: String? = null

    //@ApiModelProperty("传感器信息")
    @JvmField
    var sensorInfo: String? = null

    //@ApiModelProperty("开机时间到现在的毫秒数")
    @JvmField
    var timeToPresent: String? = null

    //@ApiModelProperty("最后⼀次启动时间")
    @JvmField
    var lastStartTime: String? = null

    //@ApiModelProperty("连接到设备的键盘的种类")
    @JvmField
    var keyboardType: String? = null

    //@ApiModelProperty("手机的信号强度")
    @JvmField
    var bssid: String? = null

    //@ApiModelProperty("是否 USB 充电 0:否，1：是")
    @JvmField
    var whetherUsbCharging: String? = null

    //@ApiModelProperty("是否交流充电")
    @JvmField
    var whetherAcCharging: String? = null

    //@ApiModelProperty("音频内部文件个数")
    @JvmField
    var internalAudioFilesNum: String? = null

    //@ApiModelProperty("图片内部文件个数")
    @JvmField
    var internalPicFilesNum: String? = null

    //@ApiModelProperty("视频内部文件个数 （从这里开始及以上的字段是2.0版本新增的）")
    @JvmField
    var internalVideoFilesNum: String? = null

    //@ApiModelProperty("wifi数量")
    @JvmField
    var wifiCount: String? = null

    //@ApiModelProperty("当前wifi的名称")
    @JvmField
    var wifiName: String? = null
    @JvmField
    var configuredWifi: List<WifiData>? = null

    //app最大占用内存
    @JvmField
    var appMaxMemory: String? = null

    //app当前可用内存
    @JvmField
    var appAvailableMemory: String? = null

    //app可释放内存
    @JvmField
    var appFreeMemory: String? = null

    class WifiData {
        @JvmField
        var bssid: String? = null
        @JvmField
        var ssid: String? = null
        @JvmField
        var wifiMac: String? = null
        @JvmField
        var wifiName: String? = null
    }
}