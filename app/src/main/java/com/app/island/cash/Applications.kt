package com.app.island.cash

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerProperties
import com.app.island.cash.params.AppParams
import com.app.island.cash.requestrisk.newwork.DataManager
import com.app.island.cash.utilstools.ActivityManager
import com.app.island.cash.utilstools.SystemUtils.getAndroidID
import com.app.island.cash.utilstools.SystemUtils.serialNumber
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.tencent.mmkv.MMKV

class Applications : Application() {
    override fun onCreate() {
        super.onCreate()
        mContext = this
        val formatStrategy: FormatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false) // (Optional) Whether to show thread info or not. Default true
            .methodCount(0) // (Optional) How many method line to show. Default 2
            .methodOffset(1) // (Optional) Hides internal method calls up to offset. Default 5
            .tag(getString(R.string.app_name)) // (Optional) Global tag for every log. Default PRETTY_LOGGER
            .build()
        Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
        MMKV.initialize(this)
        initAF()
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
                ActivityManager.instance.addActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {}
            override fun onActivityResumed(activity: Activity) {}
            override fun onActivityPaused(activity: Activity) {}
            override fun onActivityStopped(activity: Activity) {}
            override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {}
            override fun onActivityDestroyed(activity: Activity) {
                ActivityManager.instance.removeActivity(activity)
            }
        })
    }

    private fun initAF() {
        val conversionDataListener: AppsFlyerConversionListener =
            object : AppsFlyerConversionListener {
                override fun onConversionDataSuccess(map: Map<String, Any>) {}
                override fun onConversionDataFail(s: String) {}
                override fun onAppOpenAttribution(map: Map<String, String>) {}
                override fun onAttributionFailure(s: String) {}
            }
        AppsFlyerLib.getInstance().init("BP3zFL7hgXXUiVioJ5TcJe", conversionDataListener, this)
        AppsFlyerLib.getInstance().setAndroidIdData(getAndroidID(this))
        val cuid = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID)
        if (cuid == null || cuid.isEmpty()) {
            AppsFlyerLib.getInstance().setCustomerUserId(serialNumber)
        }
        AppsFlyerLib.getInstance().start(this)
        DataManager.instance.init(this, AppParams.sdk_id, AppParams.sdk_secret)
    }

    companion object {
        var mContext: Context? = null
    }
}