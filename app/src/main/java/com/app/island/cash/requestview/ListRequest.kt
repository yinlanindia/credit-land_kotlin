package com.app.island.cash.requestview

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.app.island.cash.acts.Renzhengzhogxin
import com.app.island.cash.acts.OrderDetailActivity
import com.app.island.cash.acts.Shenqingqueren
import com.app.island.cash.adapter.HomeAdapter_
import com.app.island.cash.data.list.HomeListBack
import com.app.island.cash.data.list.Product
import com.app.island.cash.databinding.ListFragBinding
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.wegit.MoreLoanDataDialog

class ListRequest {
    private var context: Activity
    private var dataBinding: ListFragBinding
    private var homeListBack: HomeListBack? = null
    private var mAdapter: HomeAdapter_
    private val listener =object :HomeAdapter_.JumpListener{
        override fun jumpActivity(productListBean: Product) {
            startAct(productListBean)
        }
    }

    constructor(contxt: FragmentActivity,binding: ListFragBinding){
        dataBinding=binding
        context = contxt
        mAdapter= HomeAdapter_(context,listener)
        dataBinding.listProduct.adapter = mAdapter
        dataBinding.listProduct.layoutManager = LinearLayoutManager(context)
        dataBinding.refresh.setOnRefreshListener {
            getListData()
        }
        getListData()
    }

    fun getListData() {
        NetWorkLoadingUtil.showDialog(context)
        val indexMarket = NetClient.getService(
            AppService::class.java
        ).findIndexMarket()
        indexMarket.enqueue(object : RequestCallBack<HttpResult<HomeListBack>>() {
            override fun onError(code: Int, msg: String) {
                super.onError(code, msg)
                dataBinding.refresh.isRefreshing = false
            }

            override fun onSuccess(body: HttpResult<HomeListBack>?) {
                dataBinding.refresh.isRefreshing = false
                homeListBack = body?.data
                val productList: List<Product>? = homeListBack?.productList
                if (!productList.isNullOrEmpty()) {
                    fillPosition(productList[0])
                    if (productList.size > 1) {
                        val beans = productList.subList(1, productList.size)
                        setAdapterData(beans)
                    }
                }
            }
        })
    }

    private fun setAdapterData(beans: List<Product>) {
        mAdapter.setProductData(beans)
    }

    private fun fillPosition(product: Product) {
        Glide.with(context).load(product.productLogo)
            .into(dataBinding.icProductLogo)
        dataBinding.tvTitle.setText(product.productName)
        dataBinding.productName.setText(product.productAmount)
//        产品状态 1、可申请 2、 申请已满 3、审核中 4、审核拒绝 5、不展示  6 审核通过 7 放款中  8 待还款 9 逾期
        //        产品状态 1、可申请 2、 申请已满 3、审核中 4、审核拒绝 5、不展示  6 审核通过 7 放款中  8 待还款 9 逾期
        if (product.productStatus == 1) {
            dataBinding.btnApply.text = "Apply"
        } else if (product.productStatus == 10) {
            dataBinding.btnApply.text = "Apply Again"
        } else if (product.productStatus == 2 || product.productStatus == 4) {
            dataBinding.btnApply.text = if (product.productStatus == 2) "Full" else "Rejected"
        } else if (product.productStatus == 3 || product.productStatus == 6 || product.productStatus == 7) {
            dataBinding.btnApply.text =
                if (product.productStatus == 3) "Reviewing" else if (product.productStatus == 6) "Approved" else "Disbursing"
        } else {
            dataBinding.btnApply.text = "Repay Now"
        }
        dataBinding.btnApply.setOnClickListener(View.OnClickListener {
            if (product.productStatus != 2) {
                startAct(product)
            }
        })
    }

    private fun startAct(productListBean: Product) {
        homeListBack?.let {
            if (it.auth?.qualified != 1) {
                context.startActivity(Intent(context, Renzhengzhogxin::class.java))
            } else {
                if (productListBean.productStatus == 1 || productListBean.productStatus == 10) {
                    NetWorkLoadingUtil.showDialog(context)
                    val httpResultCall = NetClient.getService(
                        AppService::class.java
                    ).canApplyMore()
                    httpResultCall.enqueue(object : RequestCallBack<HttpResult<Any>?>() {
                        override fun onError(code: Int, msg: String) {
                            if (code == 501) {
                                MoreLoanDataDialog(context, msg).show()
                            } else {
                                super.onError(code, msg)
                            }
                        }

                        override fun onSuccess(body: HttpResult<Any>?) {
                            val intent = Intent(
                                context,
                                Shenqingqueren::class.java
                            )
                            intent.putExtra("name", productListBean.productName)
                            context.startActivity(intent)
                        }
                    })
                } else {
                    val intent = Intent(
                        context,
                        OrderDetailActivity::class.java
                    )
                    intent.putExtra("id", productListBean.borrowId)
                    intent.putExtra("catch", productListBean.needCatch)
                    context.startActivity(intent)
                }
            }
        }
    }
}