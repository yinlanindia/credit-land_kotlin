package com.app.island.cash.wegit

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.MotionEvent
import com.app.island.cash.R

/**
 * Author: TinhoXu
 * E-mail: xth@duandai.com
 * Date: 2017/12/15 17:14
 *
 *
 * Description:
 */
class PasswordEditText : ClearEditText {
    // 资源
    private val INVISIBLE = R.drawable.icon_cantsee
    private val VISIBLE = R.drawable.icon_see

    // 按钮宽度dp
    private var mWidth = 0

    // 按钮的bitmap
    private var mBitmapInvisible: Bitmap? = null
    private var mBitmapVisible: Bitmap? = null

    // 按钮是否可见
    private var isVisible = false

    // 清除按钮出现动画
    private var mAnimatorVisible: ValueAnimator? = null

    // 消失动画
    private var mAnimatorGone: ValueAnimator? = null

    // 间隔
    private var jiiange:Int = 0

    // 内容是否是明文
    private var isPlaintext = false

    // 是否只在获得焦点后显示
    private var focusedShow = false
    private var rect: Rect? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        setSingleLine()
        // 设置EditText文本为隐藏的(注意！需要在setSingleLine()之后调用)
        transformationMethod = PasswordTransformationMethod.getInstance()
        mWidth = widthClear + context.resources.getDimension(R.dimen.dp_5)
            .toInt()
        jiiange = interval
        addRight(mWidth + jiiange)
        mBitmapInvisible = createBitmap(INVISIBLE, context)
        mBitmapVisible = createBitmap(VISIBLE, context)
        mAnimatorVisible = ValueAnimator.ofFloat(0f, 1f).setDuration(ANIMATOR_TIME.toLong())
        mAnimatorGone = ValueAnimator.ofFloat(1f, 0f).setDuration(ANIMATOR_TIME.toLong())
        rect = Rect()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (mAnimatorVisible!!.isRunning) {
            val scale = mAnimatorGone!!.animatedValue as Float
            drawVisible(scale, canvas)
            invalidate()
        } else if (isVisible) {
            drawVisible(1f, canvas)
        }
        if (mAnimatorGone!!.isRunning) {
            val scale = mAnimatorGone!!.animatedValue as Float
            drawVisible(scale, canvas)
            invalidate()
        }
    }

    /**
     * 绘制内容是否是明文的ICON
     */
    private fun drawVisible(scale: Float, canvas: Canvas) {
        val right =
            (width + scrollX - getmPaddingRight() - jiiange - mWidth * (1f - scale) / 2f).toInt()
        val left =
            (width + scrollX - getmPaddingRight() - jiiange - mWidth * (scale + (1f - scale) / 2f)).toInt()
        val top = ((height - mWidth * scale) / 2).toInt()
        val bottom = (top + mWidth * scale).toInt()
        rect!![left, top, right] = bottom
        if (isPlaintext) {
            canvas.drawBitmap(mBitmapVisible!!, null, rect!!, null)
        } else {
            canvas.drawBitmap(mBitmapInvisible!!, null, rect!!, null)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            val touchable = (width - getmPaddingRight() - mWidth - jiiange < event.x
                    && event.x < width - getmPaddingRight() - jiiange)
            if (touchable && isVisible) {
                isPlaintext = !isPlaintext
                transformationMethod = if (isPlaintext) {
                    // 设置EditText文本为可见的
                    HideReturnsTransformationMethod.getInstance()
                } else {
                    // 设置EditText文本为隐藏的
                    PasswordTransformationMethod.getInstance()
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focusedShow) {
            focusedShow(focused)
        } else {
            if (!isVisible) {
                isVisible = true
                mAnimatorGone!!.end()
                mAnimatorVisible!!.end()
                mAnimatorVisible!!.start()
                invalidate()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (focusedShow) {
            focusedShow()
        } else {
            if (!isVisible) {
                isVisible = true
                mAnimatorGone!!.end()
                mAnimatorVisible!!.end()
                mAnimatorVisible!!.start()
                invalidate()
            }
        }
    }
    /**
     * 只在获得焦点后显示
     */
    /**
     * 只在获得焦点后显示
     */
    private fun focusedShow(focused: Boolean = isFocusable) {
        if (!TextUtils.isEmpty(text) && isEditThis && focused) {
            if (!isVisible) {
                isVisible = true
                mAnimatorGone!!.end()
                mAnimatorVisible!!.end()
                mAnimatorVisible!!.start()
                invalidate()
            }
        } else {
            if (isVisible) {
                isVisible = false
                mAnimatorGone!!.end()
                mAnimatorVisible!!.end()
                mAnimatorGone!!.start()
                invalidate()
            }
        }
    }

    fun setFocusedShow(focusedShow: Boolean) {
        this.focusedShow = focusedShow
    }
}