package com.app.island.cash.requestwork

interface UrlData {
    companion object {
        const val indexMarket =
            "/e1ce809188ce838e93938e96ce87888f85a88f858499ac80938a8495cf89958c" // /api/borrow/findIndexMarket.htm
        const val moreLoan =
            "/d0ffb1a0b9ffb2bfa2a2bfa7ffb3b8b5b3bb93b1be9dbfa2b5feb8a4bd" // /api/borrow/checkCanMore.htm
        const val urlList = "/183779687137702d3774716b6c36706c75" // /api/h5/list.htm
        const val userState =
            "/0d226c7d64226c6e79226064636822787e687f4c787965226a6879587e687f4c78796523657960" // /api/act/mine/userAuth/getUserAuth.htm
        const val repayDetail =
            "/775816071e5816140358051207160e581012033e191118591f031a" // /api/act/repay/getInfo.htm
        const val findAll =
            "/5c733d2c35733d3f28733e332e2e332b733a3532381d303072342831" // /api/act/borrow/findAll.htm
        const val pageData =
            "/496628392066282a3d662420272c662b263b3b263e6639282e2c67213d24" // /api/act/mine/borrow/page.htm
        const val findBorrow =
            "/745b15041d5b1517005b161b06061b035b121d1a10361b06061b035a1c0019" // /api/act/borrow/findBorrow.htm
        const val imageSacn =
            "/a28dc3d2cb8dc3c1d68dcfcbccc78dcdc1d08dcbcfc3c5c7f1c1c3cc8ccad6cf" // /api/act/mine/ocr/imageScan.htm
        const val sdkStatus =
            "/072866776e286664732860627354636c556277687573547366737274496270296f736a" // /api/act/getSdkReportStatusNew.htm
        const val nameSave =
            "/e9c6889980c6888a9dc68480878cc69c9a8c9ba0878f86c69b8c8885a788848ca89c9d81ba889f8cc7819d84" // /api/act/mine/userInfo/realNameAuthSave.htm
        const val faceTure =
            "/381759485117595b4c175551565d175e595b5d175b575548594a514b575616504c55" // /api/act/mine/face/comparison.htm
        const val liveScore =
            "/a38cc2d3ca8cc2c0d78ccecacdc68ccfcad5c6cdc6d0d08cc4c6d7f0c0ccd1c68dcbd7ce" // /api/act/mine/liveness/getScore.htm
        const val authRealName =
            "/311e5041581e5052451e5c585f541e44425443785f575e1e5654456354505d7f505c5470444559785f575e1f59455c" // /api/act/mine/userInfo/getRealNameAuthInfo.htm
        const val dicList =
            "/527d33223b7d3331267d363b31267d3e3b21267c3a263f" // /api/act/dict/list.htm
        const val infoSave =
            "/200f4150490f4143540f4d494e450f55534552694e464f0f504552534f4e414c694e464f61555448734156450e48544d" // /api/act/mine/userInfo/personalInfoAuthSave.htm
        const val applyINfo =
            "/d4fbb5a4bdfbb6bba6a6bba3fbb7bbbab2bda6b9fabca0b9" // /api/borrow/confirm.htm
        const val applyLoan =
            "/022d63726b2d6361762d606d70706d752d716374674f63706967762c6a766f" // /api/act/borrow/saveMarket.htm
        const val getUserInfo =
            "/9bb4faebf2b4faf8efb4f6f2f5feb4eee8fee9d2f5fdf4b4ebfee9e8f4f5faf7d2f5fdf4daeeeff3d2f5fdf4b5f3eff6" // /api/act/mine/userInfo/personalInfoAuthInfo.htm
        const val contactListData =
            "/81aee0f1e8aee0e2f5aeece8efe4aee2eeeff5e0e2f5aee6e4f5c2eeeff5e0e2f5c8efe7eecde8f2f5afe9f5ec" // /api/act/mine/contact/getContactInfoList.htm
        const val saveContact =
            "/95baf4e5fcbaf4f6e1baf8fcfbf0baf6fafbe1f4f6e1bae6f4e3f0dae7c0e5f1f4e1f0bbfde1f8" // /api/act/mine/contact/saveOrUpdate.htm
        const val bankIfsc =
            "/b996d8c9d096d8dacd96dbd8d7d2f8dadad6ccd7cd96dbd8d7d2fbcbd8d7dad197d1cdd4" // /api/act/bankAccount/bankBranch.htm
        const val saveBankInfo =
            "/496628392066282a3d662b282722082a2a263c273d662b20272d20272e67213d24" // /api/act/bankAccount/binding.htm
        const val bankListData =
            "/2a054b5a43054b495e05484b44416b4949455f445e0543444c4504425e47" // /api/act/bankAccount/info.htm
        const val defaultData =
            "/2849584107494b5c074a494643694b4b475d465c075b4d5c6c4d4e495d445c06405c45" // api/act/bankAccount/setDefault.htm
        const val havePhone =
            "/6f400e1f06401a1c0a1d40061c3f0700010a2a17061c1b1c41071b02" // /api/user/isPhoneExists.htm
        const val login = "/92bdf3e2fbbde7e1f7e0bdfefdf5fbfcbcfae6ff" // /api/user/login.htm
        const val sendSms = "/e1ce809188ce94928493ce92848f85b28c92cf89958c" // /api/user/sendSms.htm
        const val register =
            "/fad59b8a93d58f899f88d5889f9d93898e9f88d4928e97" // /api/user/register.htm
        const val changePwd =
            "/644b05140d4b0507104b111701164b070c050a0301280b030d0a3413004a0c1009" // /api/act/user/changeLoginPwd.htm
        const val feedback =
            "/e4cb85948dcb858790cb898d8a81cb8b948d8a8d8b8acb979186898d90ca8c9089" // /api/act/mine/opinion/submit.htm
        const val forgetPwd =
            "/4f602e3f26603a3c2a3d6023202826216029203d282a3b1f382b61273b22" // /api/user/login/forgetPwd.htm
    }
}