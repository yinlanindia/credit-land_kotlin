package com.app.island.cash.requestview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.app.island.cash.acts.*
import com.app.island.cash.databinding.RenzhengCenterActBinding
import com.app.island.cash.data.sigle.AuthStateRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.ToastUtil

class RenzhengReq {
    private var context: AppCompatActivity
    private var binding: RenzhengCenterActBinding

    constructor(contect: AppCompatActivity, binding: RenzhengCenterActBinding) {
        this.context = contect
        this.binding = binding
        binding.tvPageTitle.text="Identity Verification"
        binding.ivRc.isEnabled = false
        binding.ivOcr.isEnabled = false
        binding.ivInfo.isEnabled = false
        binding.ivBank.isEnabled = false
        setClick()
    }

    private fun setClick() {
        binding.ivFinish.setOnClickListener { context.finish() }
        binding.swipe.setOnRefreshListener(OnRefreshListener { huoqurenzhengzhuangtai() })
        binding.llOcr.setOnClickListener {
            val intent = Intent(
                context,
                Renzheng1::class.java
            )
            intent.putExtra("state", binding.ivOcr.isEnabled())
            context.startActivity(intent)
        }
        binding.llInfo.setOnClickListener {
            if (!binding.ivOcr.isEnabled) {
                ToastUtil.toast("please complete in order")
            } else {
                val intent1 = Intent(
                    context,
                    Renzheng2::class.java
                )
                intent1.putExtra("state", binding.ivInfo.isEnabled)
                context.startActivity(intent1)
            }
        }
        binding.llRc.setOnClickListener {
            if (!binding.ivOcr.isEnabled || !binding.ivInfo.isEnabled) {
                ToastUtil.toast("please complete in order")
            } else {
                val intent2 = Intent(
                    context,
                    Renzheng3::class.java
                )
                intent2.putExtra("state", binding.ivRc.isEnabled)
                context.startActivity(intent2)
            }
        }
        binding.llBank.setOnClickListener {
            if (!binding.ivOcr.isEnabled || !binding.ivInfo.isEnabled || !binding.ivRc.isEnabled) {
                ToastUtil.toast("please complete in order")
            } else {
                if (binding.ivBank.isEnabled) {
                    context.startActivity(Intent(context, AccountMore::class.java))
                } else {
                    context.startActivity(Intent(context, AccountNew::class.java))
                }
            }
        }
    }

     fun huoqurenzhengzhuangtai() {
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).userAuthState().enqueue(object : RequestCallBack<HttpResult<AuthStateRec>?>() {
            override fun onSuccess(body: HttpResult<AuthStateRec>?) {
                binding.swipe.isRefreshing = false
                sadakh(body)
            }

            override fun onError(code: Int, msg: String) {
                super.onError(code, msg)
                binding.swipe.setRefreshing(false)
            }
        })
    }

    private fun sadakh(body: HttpResult<AuthStateRec>?) {
        binding.ivBank.isEnabled = body?.data?.bankCardState == "30"
        binding.ivInfo.isEnabled = "30" == body?.data?.personalInfoState
        binding.ivOcr.isEnabled = "30" == body?.data?.realNameAuthState
        binding.ivRc.isEnabled = "30" == body?.data?.contactState
    }
}