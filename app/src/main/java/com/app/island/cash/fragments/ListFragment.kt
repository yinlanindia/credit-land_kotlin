package com.app.island.cash.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.island.cash.R
import com.app.island.cash.databinding.ListFragBinding
import com.app.island.cash.requestview.ListRequest

class ListFragment:Fragment() {

    private lateinit var listFragBinding:ListFragBinding
    private lateinit var listView:ListRequest
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        listFragBinding= DataBindingUtil.inflate(inflater, R.layout.list_frag, container, false);
        return listFragBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView = ListRequest(requireActivity(),listFragBinding)
    }

    fun reqHomeData() {
        listView.getListData()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            reqHomeData()
        }
    }

}