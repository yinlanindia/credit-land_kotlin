package com.app.island.cash.requestview

import android.content.Intent
import android.view.View
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.R
import com.app.island.cash.acts.ApplySuccActivity
import com.app.island.cash.acts.HtmlView
import com.app.island.cash.databinding.ShenqingQuerenActBinding
import com.app.island.cash.data.sigle.AppluSubmitCall
import com.app.island.cash.data.sigle.ProductionReq
import com.app.island.cash.data.sigle.ApplySubmitBack
import com.app.island.cash.data.sigle.ProductResp
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.params.AppParams
import com.app.island.cash.requestrisk.newwork.DataManager
import com.app.island.cash.utilstools.*
import com.app.island.cash.wegit.CatchDialog
import com.app.island.cash.wegit.MoreLoanDataDialog

class ShenqingquerenReq {
    private val context: AppCompatActivity
    private val binding: ShenqingQuerenActBinding
    private val name: String

    constructor(context: AppCompatActivity, binding: ShenqingQuerenActBinding, name: String) {
        this.context = context
        this.binding = binding
        this.name = name
        binding.tvPageTitle.text = "Loan Application"
        huoqujibenxinxi(name)
        shezhijianting()
    }

    private fun shezhijianting() {
        binding.tvAgreementLoan.setOnClickListener(View.OnClickListener {
            if (!shuomingUrl.isNullOrEmpty()) {
                val intent = Intent(
                    context,
                    HtmlView::class.java
                )
                intent.putExtra("url", shuomingUrl)
                intent.putExtra("title", "Loan Agreement")
                context.startActivity(intent)
            }
        })
        binding.agree.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            binding.agree.isChecked = b
        })
        binding.btnSubmit.setOnClickListener(View.OnClickListener {
            if (binding.agree.isChecked) {
                shenqingjiekuan()
            } else {
                ToastUtil.toast("Please read and agree 《Loan Agreement》")
            }
        })
        binding.ivInterest.setOnClickListener(View.OnClickListener {
            MaterialDialogUtils.showBasicDialogNoCancel(
                context,
                "Interest Fee",
                "A nominal interest will be charged on per day basis depending upon the loan duration and amount."
            ).build().show()
        })
        binding.ivProcessing.setOnClickListener(View.OnClickListener {
            MaterialDialogUtils.showBasicDialogNoCancel(
                context,
                "Processing Fee",
                "Including but not limited to: Bank charges,adiministrative charges for processing the loan application. The same would be deducted from the Loan Amount."
            ).build().show()
        })
        binding.ivAssessment.setOnClickListener(View.OnClickListener {
            MaterialDialogUtils.showBasicDialogNoCancel(
                context,
                "Assessment Fee",
                "It includes the borrowers credit check and background evaluation.These charges would be deducted from the loan amount."
            ).build().show()
        })
        binding.ivGts.setOnClickListener(View.OnClickListener {
            MaterialDialogUtils.showBasicDialogNoCancel(
                context,
                "GST Fee",
                "GST is Charged on the sales of goods and services made in india"
            ).build().show()
        })
    }

    private fun shenqingjiekuan() {
        NetWorkLoadingUtil.showDialog(context)
        val call = AppluSubmitCall(fengkongleixing!!, name, context.getString(R.string.app_name))
        call.address = AddressUtils.instance!!.detailAddress
        call.coordinate =
            AddressUtils.instance!!.latitude.toString() + "," + AddressUtils.instance!!.longitude
        call.imei = SystemUtils.getIMEI(context)
        call.ip = SystemUtils.iP
        call.appRunTime =
            "" + (System.currentTimeMillis() - UserInfoManager.instance.currentTIme)
        val resultCall = NetClient.getService(
            AppService::class.java
        ).submitApply(call)
        resultCall.enqueue(object : RequestCallBack<HttpResult<ApplySubmitBack>?>() {
            override fun onSuccess(body: HttpResult<ApplySubmitBack>?) {
                if (body?.code == 200 && body.data != null) {
                    if (body.data!!.needCatch) {
                        DataManager.instance.SynData(
                            body.data!!.borrowId!!,
                            UserInfoManager.instance.uid,
                            UserInfoManager.instance.phoneNum,
                            AppParams.sdk_url
                        )
                        CatchDialog(
                            context,
                            body.data!!.needCatchWaitingTime,
                            object : CatchDialog.DownFinishListener {
                                override fun castchSucce() {
                                    context.startActivity(
                                        Intent(
                                            context,
                                            ApplySuccActivity::class.java
                                        )
                                    )
                                    context.finish()
                                }
                            }).show()
                    } else {
                        context.startActivity(
                            Intent(
                                context,
                                ApplySuccActivity::class.java
                            )
                        )
                        context.finish()
                    }
                } else {
                    MoreLoanDataDialog(context, body?.msg!!).show()
                }
            }
        })
    }

    private fun huoqujibenxinxi(name: String) {
        val req = ProductionReq(name)
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).getProductionInfo(req).enqueue(object : RequestCallBack<HttpResult<ProductResp>?>() {
            override fun onSuccess(body: HttpResult<ProductResp>?) {
                tianchongshuju(body?.data)
            }
        })
    }

    private var fengkongleixing: String? = null
    private var shuomingUrl: String? = null
    private fun tianchongshuju(data: ProductResp?) {
        data?.let {
            fengkongleixing = it.riskType
            binding.tvLoan.text = it.amount
            binding.tvName.text = name
            binding.tvDisbursal.text = it.realAmount
            binding.tvRepayment.text = it.repaymentAmount
            binding.tvApplication.text = it.applicationDate
            binding.tvDue.text = it.repayTime
            binding.tvBankName.text = it.bankName
            binding.tvBankNum.text = it.bankAccount
            binding.tvInterest.text = it.interest
            binding.tvProcessing.text = it.onceHandlingFee
            binding.tvAssessment.text = it.onceServiceFee
            binding.tvGts.text = it.gstFee
            shuomingUrl =
                AppParams.net_url + "/protocol/protocolPreview.htm?amount=" + it.amount + "&userId=" + UserInfoManager.instance.uid +
                        "&timeLimit=" + it.dayLimit + "&appFlag=" + AppParams.flag + "&productName=" + name
        }
    }
}