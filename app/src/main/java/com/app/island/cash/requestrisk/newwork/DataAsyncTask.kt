package com.app.island.cash.requestrisk.newwork

import android.os.AsyncTask
import android.text.TextUtils
import com.app.island.cash.requestrisk.newwork.StatisticLogManage.HttpStateException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Date: 2020/5/25 下午4:01
 * Author: kay lau
 * Description:
 */
class DataAsyncTask(
    private val reqUrl: String,
    private val reqJsonStr: String,
    private val signMsg: String
) : AsyncTask<Void?, Void?, Void?>() {
    override fun doInBackground(vararg p0: Void?): Void? {
        if (TextUtils.isEmpty(reqUrl)) {
            LogUtil.e("reqUrl = null")
            return null
        }
        var result: String? = null
        try {
            result = StatisticLogManage.statisticLog(reqUrl, reqJsonStr, signMsg)
            LogUtil.e("req result: $result")
        } catch (e: UnknownHostException) {
            LogUtil.e("UnknownHostException doInBackground: $e")
        } catch (e: SocketTimeoutException) {
            LogUtil.e("SocketTimeoutException doInBackground: $e")
        } catch (e: HttpStateException) {
            LogUtil.e("Exception doInBackground: $e")
        } catch (e: Exception) {
            LogUtil.e("doInBackground: $e")
        }
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
    }

    companion object {
        private val TAG = DataAsyncTask::class.java.simpleName
    }
}