package com.app.island.cash.params

object AppParams {
//    private const val debug_url = "http://appsaas-api.yinlas.com"

    private const val  debug_url = "";
    private const val release_url = "https://api.credit-island.in"
    const val beta = false
    val net_url = if (beta) debug_url else release_url
    private const val debug_sdk_id = "CreditIsland"
    private const val release_sdk_id = "CreditIsland"

    @JvmField
    val sdk_id = if (beta) debug_sdk_id else release_sdk_id
    private const val debug_sdk_url = ""
//    private const val debug_sdk_url = "http://test-sdk.yinlas.com";
    private const val release_sdk_url = "https://sdk.credit-island.in"
    val sdk_url = if (beta) debug_sdk_url else release_sdk_url
    private const val debug_sdk_secret = "hIdukN1Q95d8Lwa6155vjJ8OuuOrZ8EH"
    private const val release_sdk_secret = "Ga730ws3GBO35C6a5ICRwCnEt8PGX48z"

    @JvmField
    val sdk_secret = if (beta) debug_sdk_secret else release_sdk_secret
    const val channel = "google"
    const val flag = "100019"
    const val SIGN_KEY_TEST = "tTKCMaugwk1QDf5mBdRlxIM9CAsuORWH"
    const val SIGN_KEY_RELEASE = "Ga730ws3GBO35C6a5ICRwCnEt8PGX48z"
    val SIGN_KEY = if (beta) SIGN_KEY_TEST else SIGN_KEY_RELEASE
}