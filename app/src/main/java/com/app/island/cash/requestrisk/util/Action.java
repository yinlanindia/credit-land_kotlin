package com.app.island.cash.requestrisk.util;

public interface Action<T> {
    void call(T t);
}
