package com.app.island.cash.requestview

import androidx.appcompat.app.AppCompatActivity
import com.app.island.cash.databinding.RenzhengXinxiiActBinding
import com.app.island.cash.data.sigle.SaveUInfoCall
import com.app.island.cash.data.sigle.SelectListDataRec
import com.app.island.cash.data.sigle.UserInfoRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.NetWorkLoadingUtil
import com.app.island.cash.utilstools.ToastUtil
import com.app.island.cash.wegit.ListSelectDialog

class Renzheng2Req {
    private var xuanzeshuju: SelectListDataRec? = null
    fun huoquxinxi() {
        NetClient.getService(
            AppService::class.java
        ).userInfo().enqueue(object : RequestCallBack<HttpResult<UserInfoRec>?>() {
            override fun onSuccess(body: HttpResult<UserInfoRec>?) {
                val data = body?.data
                tianchongshuju(data)

            }
        })
    }

    private fun tianchongshuju(data: UserInfoRec?) {
        data?.let {
            binding.firstName.setText(it.firstName)
            binding.middleName.setText(it.middleName)
            binding.lastName.setText(it.lastName)
            binding.email.setText(it.email)
            binding.education.text = it.education
            binding.accommodation.text = it.accommodationType
            binding.marriey.text = it.marital
            binding.children.text = it.childrenNumber
            binding.purpose.text = it.loanPurpose
            binding.occupation.text = it.occupation
            binding.salary.text = it.salary
            binding.pay.text = it.salaryType
        }
    }

    fun huoquxuanzeshuju(type: String) {
        NetClient.getService(
            AppService::class.java
        ).getDicList(type).enqueue(object : RequestCallBack<HttpResult<SelectListDataRec>?>() {
            override fun onSuccess(body: HttpResult<SelectListDataRec>?) {
                xuanzeshuju = body?.data
            }
        })
    }

    private val context: AppCompatActivity
    private val binding: RenzhengXinxiiActBinding
    private var fangwuId: Int = 0
    private var haiziId: Int = 0
    private var jiaoiyuId: Int = 0
    private var hunyinId: Int = 0
    private var zhiweiId: Int = 0
    private var zhifuId: Int = 0

    constructor(context: AppCompatActivity, binding: RenzhengXinxiiActBinding, state: Boolean) {
        this.context = context;
        this.binding = binding
        if (!state) {
            binding.accommodation.setOnClickListener {
                ListSelectDialog(
                    context,
                    xuanzeshuju?.accommodationTypeList,
                    fangwuId
                )
                    .setOnGenderDialogListener(object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                            fangwuId = selectedIndex
                            binding.accommodation.text =xuanzeshuju?.accommodationTypeList?.get(selectedIndex)?.value
                        }
                    }).show()
            }
            binding.children.setOnClickListener {
                ListSelectDialog(
                    context,
                    xuanzeshuju?.childrenNumberList,
                    haiziId
                )
                    .setOnGenderDialogListener (object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                            haiziId = selectedIndex
                            binding.children.text =
                                xuanzeshuju?.childrenNumberList?.get(selectedIndex)?.value
                        }}).show()
            }
            binding.education.setOnClickListener {
                ListSelectDialog(
                    context,
                    xuanzeshuju?.educationalStateList,
                    jiaoiyuId
                )
                    .setOnGenderDialogListener (object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        jiaoiyuId = selectedIndex
                        binding.education.text =
                            xuanzeshuju?.educationalStateList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.marriey.setOnClickListener {
                ListSelectDialog(context, xuanzeshuju?.maritalStateList, hunyinId)
                    .setOnGenderDialogListener(object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        hunyinId = selectedIndex
                        binding.marriey.text =
                            xuanzeshuju?.maritalStateList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.occupation.setOnClickListener {
                ListSelectDialog(context, xuanzeshuju?.positionList, zhiweiId)
                    .setOnGenderDialogListener (object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        zhiweiId = selectedIndex
                        binding.occupation.text =
                            xuanzeshuju?.positionList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.pay.setOnClickListener {
                ListSelectDialog(context, xuanzeshuju?.salaryTypeList, zhifuId)
                    .setOnGenderDialogListener(object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        zhifuId = selectedIndex
                        binding.pay.text = xuanzeshuju?.salaryTypeList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.purpose.setOnClickListener {
                ListSelectDialog(
                    context,
                    xuanzeshuju?.loanPurposeList,
                    yongtuId
                )
                    .setOnGenderDialogListener (object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        yongtuId = selectedIndex
                        binding.purpose.text =
                            xuanzeshuju?.loanPurposeList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.salary.setOnClickListener {
                ListSelectDialog(context, xuanzeshuju?.salaryRangeList, xinshuiId)
                    .setOnGenderDialogListener(object :ListSelectDialog.OnListSelectListener{
                        override fun onCallBack(selectedIndex: Int, item: String?) {
                        xinshuiId = selectedIndex
                        binding.salary.text =
                            xuanzeshuju?.salaryRangeList?.get(selectedIndex)?.value
                    }}).show()
            }
            binding.saveUser.setOnClickListener {
                baoxunxinxi()
            }
        }
    }

    private fun baoxunxinxi() {
        val namefirst: String = binding.firstName.text.toString()
        val nameMiid: String = binding.middleName.text.toString()
        val nameLast: String = binding.lastName.text.toString()
        val email: String = binding.email.text.toString()
        val education: String = binding.education.text.toString()
        val accom: String = binding.accommodation.text.toString()
        val marry: String = binding.marriey.text.toString()
        val childer: String = binding.children.text.toString()
        val purpose: String = binding.purpose.text.toString()
        val occu: String = binding.occupation.text.toString()
        val salary: String = binding.salary.text.toString()
        val pay: String = binding.pay.text.toString()
        if (namefirst.isEmpty()) {
            ToastUtil.toast("please enter your first name")
            return
        }
        if (nameLast.isEmpty()) {
            ToastUtil.toast("please enter your last name")
            return
        }
        if (email.isEmpty()) {
            ToastUtil.toast("please enter your email")
            return
        }
        if (education.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvEducational.text)
            return
        }
        if (accom.isEmpty()) {
            ToastUtil.toast("please select your " + binding.TypeOfAccommodation.text)
            return
        }
        if (marry.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvMaritalStatus.text)
            return
        }
        if (childer.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvChildren.text)
            return
        }
        if (purpose.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvPurpose.text)
            return
        }
        if (occu.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvOccupation.text)
            return
        }
        if (salary.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvSalary.text)
            return
        }
        if (pay.isEmpty()) {
            ToastUtil.toast("please select your " + binding.tvMethod.text)
            return
        }
        val call = SaveUInfoCall(
            email, nameMiid, namefirst, nameLast, marry, education, accom, childer, purpose, occu,
            salary, pay
        )
        baocun(call)
    }

    private fun baocun(call: SaveUInfoCall) {
        NetWorkLoadingUtil.showDialog(context)
        val resultCall = NetClient.getService(
            AppService::class.java
        ).saveUInfo(call)
        resultCall.enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                ToastUtil.toast(body?.msg)
                context.finish()
            }
        })
    }

    private var yongtuId: Int = 0
    private var xinshuiId: Int = 0
}