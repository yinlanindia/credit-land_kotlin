package com.app.island.cash.wegit

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.app.island.cash.R

class MoreLoanDataDialog(context: Context, private val date: String) :
    Dialog(context, R.style.loading_dialog) {
    private lateinit var dataTv: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.more_loan_layout)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        val spannableString =
            SpannableString("Maintain good repayments of Credit Island Card ($date)")
        spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, 46, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            AbsoluteSizeSpan(10, true),
            47,
            spannableString.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
        dataTv = findViewById(R.id.tv_tips)
        dataTv.setText(spannableString)
        // 加载动画
        window!!.attributes.gravity = Gravity.CENTER //居中显示
        window!!.attributes.dimAmount = 0.5f //背景透明度 取值范围 0 ~ 1
        findViewById<View>(R.id.repay_button).setOnClickListener { dismiss() }
    }
}