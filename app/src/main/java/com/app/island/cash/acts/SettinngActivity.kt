package com.app.island.cash.acts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.afollestad.materialdialogs.MaterialDialog
import com.app.island.cash.R
import com.app.island.cash.databinding.ActivitySettingBinding
import com.app.island.cash.data.sigle.UrlListRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.params.AppParams
import com.app.island.cash.utilstools.ActivityManager
import com.app.island.cash.utilstools.UserInfoManager

class SettinngActivity : BaseAct() {
    private lateinit var binding: ActivitySettingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_setting)
        binding.tvPageTitle.text="Settings"
        initListener()
        initData()
    }

    private var privacyUtl: String? = null
    private var termsUrl: String? = null
   

     fun initListener() {
         binding.ivFinish.setOnClickListener { finish() }
        binding.tvLogout.setOnClickListener {
            MaterialDialog.Builder(this@SettinngActivity)
                .content("Are you sure to quit?")
                .cancelable(false)
                .positiveText("Quit")
                .onPositive { dialog, which ->
                    dialog.dismiss()
                    UserInfoManager.instance.logout()
                    ActivityManager.instance.finishAllActivity()
                    startActivity(Intent(this@SettinngActivity, EnterStart::class.java))
                }
                .negativeText("Cancel")
                .negativeColor(resources.getColor(R.color.color_999999))
                .onNegative { dialog, which -> dialog.dismiss() }.build().show()
        }
        binding.rlChange.setOnClickListener {
            startActivity(
                Intent(
                    this@SettinngActivity,
                    ChangePasswordActivity::class.java
                )
            )
        }
        binding.rlAbout.setOnClickListener {
            startActivity(
                Intent(
                    this@SettinngActivity,
                    AboutAct::class.java
                )
            )
        }
        binding.rlFeedback.setOnClickListener {
            startActivity(
                Intent(
                    this@SettinngActivity,
                    FeedbackActivity::class.java
                )
            )
        }
        binding.rlPermission.setOnClickListener {
            val localIntent = Intent()
            localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            localIntent.action = "android.settings.APPLICATION_DETAILS_SETTINGS"
            localIntent.data = Uri.fromParts("package", packageName, null)
            startActivity(localIntent)
        }
        binding.rlPrivacy.setOnClickListener {
            val intent = Intent(this@SettinngActivity, HtmlView::class.java)
            intent.putExtra("title", "Privacy Policy")
            intent.putExtra("url", privacyUtl)
            startActivity(intent)
        }
        binding.rlTerm.setOnClickListener {
            val intent = Intent(this@SettinngActivity, HtmlView::class.java)
            intent.putExtra("title", "Terms of Use")
            intent.putExtra("url", termsUrl)
            startActivity(intent)
        }
    }

     fun initData() {
        NetClient.getService(
            AppService::class.java
        ).cOmmonUrl().enqueue(object : RequestCallBack<HttpResult<UrlListRec>?>() {
            override fun onSuccess(body: HttpResult<UrlListRec>?) {
                for (data in body?.data?.list!!) {
                    if ("h5_protocol_privacy" == data.code) {
                        privacyUtl = AppParams.net_url + data.value
                    }
                    if ("h5_protocol_register" == data.code) {
                        termsUrl = AppParams.net_url + data.value
                    }
                }
            }
        })
    }
}