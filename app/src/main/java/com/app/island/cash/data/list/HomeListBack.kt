package com.app.island.cash.data.list

data class HomeListBack(
    var auth: Auth? = Auth(),
    var productList: List<Product> = listOf()
)