package com.app.island.cash.acts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.app.island.cash.R
import com.app.island.cash.data.sigle.LogoutEvent
import com.app.island.cash.data.sigle.UpdateEvent
import com.app.island.cash.utilstools.ActivityManager
import com.app.island.cash.utilstools.UserInfoManager
import com.gyf.immersionbar.ImmersionBar
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class BaseAct:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.color_4071ff)
            .navigationBarColor(R.color.black).statusBarDarkFont(false, 0.2f).init()

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: UpdateEvent) {
        MaterialDialog.Builder(this)
            .content(event.msg)
            .cancelable(false)
            .positiveText("Confirm")
            .onPositive { dialog, which ->
                dialog.dismiss()
                val uri = Uri.parse("market://details?id=$packageName")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                intent.setPackage("com.android.vending")
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                ActivityManager.instance.finishAllActivity()
            }
            .negativeText("Cancel")
            .negativeColor(resources.getColor(R.color.color_999999))
            .onNegative { dialog, which ->
                dialog.dismiss()
                ActivityManager.instance.finishAllActivity()
            }.build().show()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onLogoutEvent(event: LogoutEvent?) {
        MaterialDialog.Builder(this)
            .content("Your account was just signed in on a new device. If it is not your operation, please change your password in time.")
            .cancelable(false)
            .positiveText("Login")
            .onPositive { dialog, which ->
                dialog.dismiss()
                UserInfoManager.instance.logout()
                ActivityManager.instance.finishAllActivity()
                startActivity(Intent(this@BaseAct, EnterStart::class.java))
            }
            .negativeText("Logout")
            .negativeColor(resources.getColor(R.color.color_999999))
            .onNegative { dialog, which ->
                dialog.dismiss()
                UserInfoManager.instance.logout()
                ActivityManager.instance.finishAllActivity()
                startActivity(Intent(this@BaseAct, EnterStart::class.java))
            }.build().show()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}