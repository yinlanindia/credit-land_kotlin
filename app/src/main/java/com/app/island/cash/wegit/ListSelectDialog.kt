package com.app.island.cash.wegit

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.island.cash.R
import com.app.island.cash.data.sigle.SelectListDataRec
import com.app.island.cash.wegit.ListSelectDialog.ListDialogAdapter.MyViewHolder

class ListSelectDialog : Dialog, View.OnClickListener {
    private var listener: OnListSelectListener? = null
    private var contxt: Context
    private var data: List<SelectListDataRec.ListItem>?
    private var selectedIndex = 0
    private var item: SelectListDataRec.ListItem? = null
    fun setSelectedIndex(selectedIndex: Int) {
        this.selectedIndex = selectedIndex
    }

    fun setItem(item: SelectListDataRec.ListItem?) {
        this.item = item
    }

    constructor(contxt: Context, data: List<SelectListDataRec.ListItem>?) : super(
        contxt,
        R.style.select_dialog
    ) {
        this.contxt = contxt
        this.data = data
    }

    constructor(
        context: Context,
        data: List<SelectListDataRec.ListItem>?,
        defaultSelect: Int
    ) : super(context, R.style.select_dialog) {
        this.contxt = context
        this.data = data
        selectedIndex = defaultSelect
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        //填充对话框的布局
        val inflate = LayoutInflater.from(context).inflate(R.layout.layout_list_dialog, null)
        //初始化控件
        inflate.findViewById<View>(R.id.tv_cancel).setOnClickListener(this)
        inflate.findViewById<View>(R.id.tv_select).setOnClickListener(this)
        val recyclerView: RecyclerView = inflate.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = ListDialogAdapter(context, data, selectedIndex)
        adapter.setListener(object : ListDialogAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                adapter.setSelectPosition(position)
                if (data != null && data!!.size > 0) {
                    setItem(data!![position])
                }
                setSelectedIndex(position)
            }
        })
        recyclerView.adapter = adapter

        //将布局设置给Dialog
        setContentView(inflate)
        initDialogAttributes()
    }

    private fun initDialogAttributes() {
        //获取当前Activity所在的窗体
        val dialogWindow = window
        //设置Dialog从窗体底部弹出
        dialogWindow!!.setGravity(Gravity.BOTTOM)
        val lp = dialogWindow.attributes
        lp.width = getScreenWidth(contxt as Activity)
        //将属性设置给窗体
        dialogWindow.attributes = lp
    }

    private fun getScreenWidth(activity: Activity): Int {
        val manager = activity.windowManager
        val outMetrics = DisplayMetrics()
        manager.defaultDisplay.getMetrics(outMetrics)
        return outMetrics.widthPixels
    }

    fun setOnGenderDialogListener(listener: OnListSelectListener?): ListSelectDialog {
        this.listener = listener
        return this
    }

    override fun show() {
        super.show()
        setSelectedIndex(selectedIndex)
        if (data != null && data!!.size > 0) {
            setItem(data!![selectedIndex])
        }
    }

    private var lastClickTime: Long = 0
    val isCanClick: Boolean
        get() = try {
            val time = System.currentTimeMillis()
            val offSetTime = time - lastClickTime
            if (Math.abs(offSetTime) > 500) {
                lastClickTime = time
                true
            } else {
                false
            }
        } catch (e: Exception) {
            true
        }

    override fun onClick(v: View) {
        if (!isCanClick) {
            return
        }
        val id = v.id
        if (id == R.id.tv_cancel) {
            dismiss()
        } else if (id == R.id.tv_select) {
            if (listener != null) {
                listener!!.onCallBack(selectedIndex, item!!.value)
            }
            dismiss()
        }
    }


    interface OnListSelectListener {
        fun onCallBack(selectedIndex: Int, item: String?)
    }

    class ListDialogAdapter(
        private val mContext: Context,
        private val datas: List<SelectListDataRec.ListItem>?,
        private var selectPosition: Int
    ) : RecyclerView.Adapter<MyViewHolder>(), View.OnClickListener {
        interface OnItemClickListener {
            fun onItemClick(view: View?, position: Int)
        }

        private var listener: OnItemClickListener? = null
        fun setSelectPosition(selectPosition: Int) {
            this.selectPosition = selectPosition
            notifyDataSetChanged()
        }

        fun setListener(listener: OnItemClickListener?) {
            this.listener = listener
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            val view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_dialog_select, parent, false)
            view.setOnClickListener(this)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            if (position == selectPosition) {
                holder.tv_title.setTextColor(mContext.resources.getColor(R.color.color_26357b))
            } else {
                holder.tv_title.setTextColor(mContext.resources.getColor(R.color.color_afafaf))
            }
            if (datas != null && position == datas.size - 1) {
                holder.vLine.visibility = View.INVISIBLE
            } else {
                holder.vLine.visibility = View.VISIBLE
            }
            if (datas != null && datas.size > 0) {
                holder.tv_title.text = datas[position].value
            }
            holder.itemView.tag = position
        }

        override fun getItemCount(): Int {
            return datas?.size ?: 0
        }

        override fun onClick(v: View) {
            listener!!.onItemClick(v, v.tag as Int)
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_title: TextView
            var vLine: View

            init {
                tv_title = view.findViewById(R.id.tv_title)
                vLine = view.findViewById(R.id.vLine)
            }
        }
    }
}