package com.app.island.cash.data.sigle

class DetailBack {
    var amount: String? = null
    var tenure: String? = null
    var bankName: String? = null
    var cardNo: String? = null
    var state: String? = null
    var applicationDate: String? = null
    var repayment: String? = null
    var repayTime: String? = null
    var canExtension = false
    var productName: String? = null
    var extensionAmount: String? = null
    var extensionFee: String? = null
    var extensionDate: String? = null
    var actualRepayTime = ""
    var actualRepayment = ""
    var list: List<ListBean>? = null

    class ListBean {
        var id = 0
        var userId = 0
        var state: String? = null
        var remark: String? = null
        var createTime: Long = 0
        var type: String? = null
        var createTimeStr: String? = null
        var str: String? = null
    }
}