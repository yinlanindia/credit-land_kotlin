package com.app.island.cash.wegit

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import com.app.island.cash.R
import com.app.island.cash.data.sigle.SelectListDataRec

class PermissionSelectDialog(private val contxt: Activity) : Dialog(
    contxt, R.style.select_dialog
), View.OnClickListener {
    private var acceptListener: OnListSelectListener? = null
    private var denyListener: OnListSelectListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        //填充对话框的布局
        val inflate = LayoutInflater.from(context).inflate(R.layout.permission_list_dialog, null)
        //初始化控件
        inflate.findViewById<View>(R.id.btn_go_home).setOnClickListener(this)
        inflate.findViewById<View>(R.id.btn_cancel).setOnClickListener(this)
        //将布局设置给Dialog
        setContentView(inflate)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        initDialogAttributes()
    }

    private fun initDialogAttributes() {
        //获取当前Activity所在的窗体
        val dialogWindow = window
        //设置Dialog从窗体底部弹出
        dialogWindow!!.setGravity(Gravity.BOTTOM)
        val lp = dialogWindow.attributes
        lp.width = getScreenWidth(contxt as Activity)
        //将属性设置给窗体
        dialogWindow.attributes = lp
    }

    private fun getScreenWidth(activity: Activity): Int {
        val manager = activity.windowManager
        val outMetrics = DisplayMetrics()
        manager.defaultDisplay.getMetrics(outMetrics)
        return outMetrics.widthPixels
    }

    fun setOnAcceptDialogListener(listener: OnListSelectListener): PermissionSelectDialog {
        acceptListener = listener
        return this
    }

    fun setOnDenyDialogListener(listener: OnListSelectListener): PermissionSelectDialog {
        denyListener = listener
        return this
    }


    private var lastClickTime: Long = 0
    val isCanClick: Boolean
        get() = try {
            val time = System.currentTimeMillis()
            val offSetTime = time - lastClickTime
            if (Math.abs(offSetTime) > 500) {
                lastClickTime = time
                true
            } else {
                false
            }
        } catch (e: Exception) {
            true
        }

    override fun onClick(v: View) {
        if (!isCanClick) {
            return
        }
        val id = v.id
        if (id == R.id.btn_cancel) {
            if (denyListener != null) {
                denyListener!!.onCallBack(this)
            }
            dismiss()
        } else if (id == R.id.btn_go_home) {
            if (acceptListener != null) {
                acceptListener!!.onCallBack(this)
            }
        }
    }

    interface OnListSelectListener {
        fun onCallBack(dialog: PermissionSelectDialog?)
    }
}