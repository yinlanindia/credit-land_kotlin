package com.app.island.cash.data.sigle

class RepayListBack {
    var list: List<Bean>? = null

    class Bean {
        val borrowId: String? = null
        val productLogo: String? = null
        val repayment: String? = null
        val applicationTime: Long = 0
        val applicationTimeStr: String? = null
        val repayTime: Long = 0
        val repayTimeStr: String? = null
        val state: String? = null
        val penaltyDay = 0
        val dueDay = 0
        val productName: String? = null
    }
}