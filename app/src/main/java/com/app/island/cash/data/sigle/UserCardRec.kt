package com.app.island.cash.data.sigle

class UserCardRec {
    var phone: String? = null
    var realName: String? = null
    var sex: String? = null
    var idNo: String? = null
    var idAddr: String? = null
    var livingImg: String? = null
    var frontImg: String? = null
    var backImg: String? = null
    var panCode: String? = null
    var panImg: String? = null
    var dateOfBirth: String? = null
    var pinCode: String? = null
}