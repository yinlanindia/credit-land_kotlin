package com.app.island.cash.requestrisk.bean

class CalendarData {
    var data: List<CalendarInfo>? = null

    class CalendarInfo {
        //提醒事件ID
        var event_id: String? = null

        //提醒事件标题
        var event_title: String? = null

        //提醒事件描述
        var description: String? = null

        //事件开始时间 毫秒时间戳
        var start_time: Long = 0

        //事件结束时间 毫秒时间戳
        var end_time: Long = 0
        var reminders: List<ReminderInfo>? = null
    }

    class ReminderInfo {
        //事件ID
        var event_id: String? = null

        //提醒方式（1：METHOD_ALERT，2：METHOD_DEFAULT，3：METHOD_EMAIL，4：METHOD_SMS）
        var method = 0

        //在事件发生之前多少分钟进行提醒
        var minutes = 0

        //提醒ID
        var reminder_id: String? = null
    }
}