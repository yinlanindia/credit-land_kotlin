package com.app.island.cash.utilstools

import android.app.Activity
import java.util.*

class ActivityManager {
    /**
     * 添加Activity到堆栈
     */
    fun addActivity(activity: Activity?) {
        if (activityStack == null) {
            activityStack = ArrayList()
        }
        activityStack!!.add(activity)
    }

    /**
     * 移除指定的Activity
     */
    fun removeActivity(activity: Activity?) {
        var activity = activity
        if (activity != null) {
            activityStack!!.remove(activity)
            activity = null
        }
    }

    /**
     * 结束所有Activity
     */
    fun finishAllActivity() {
        var i = 0
        val size = activityStack!!.size
        while (i < size) {
            if (null != activityStack!![i]) {
                activityStack!![i]!!.finish()
            }
            i++
        }
        activityStack!!.clear()
    }

    val topActivity: Activity?
        get() = activityStack!![activityStack!!.size - 1]

    companion object {
        @JvmStatic
        val instance = ActivityManager()
        private var activityStack: MutableList<Activity?>? = ArrayList()
    }
}