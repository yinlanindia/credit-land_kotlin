package com.app.island.cash.acts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.app.island.cash.data.sigle.SDKSubmitCall
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.orhanobut.logger.Logger

class DataReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val syncedType = intent.getStringExtra("SyncedType")
        val msg = intent.getStringExtra("SyncedMsg")
        val borrowId = intent.getStringExtra("SyncedBorrowId")
        val syncedState = intent.getBooleanExtra("SyncedState", false)
        val code = intent.getIntExtra("SyncedCode", 0)
        val reportStatus: Int = if (syncedState) {
            1
        } else 0
        Logger.e("SyncedType: $syncedType,  同步状态: $syncedState,  code:$code   msg: \$msg, borrowId: $borrowId")
        val call = SDKSubmitCall(
            borrowId!!,
            msg!!,
            code.toString() + "",
            reportStatus.toString() + "",
            syncedType!!
        )
        NetClient.getService(
            AppService::class.java
        ).getSdkReportStatusNew(call).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {}
            override fun onError(code: Int, msg: String) {}
        })
    }
}