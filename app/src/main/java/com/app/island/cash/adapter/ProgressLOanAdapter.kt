package com.app.island.cash.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.island.cash.R
import com.app.island.cash.adapter.ProgressLOanAdapter.Holdr
import com.app.island.cash.data.sigle.DetailBack
import java.util.*

class ProgressLOanAdapter(private val context: Context) : RecyclerView.Adapter<Holdr>() {
    private val listBeans: MutableList<DetailBack.ListBean>
    fun setListBeans(datas: List<DetailBack.ListBean>?) {
        listBeans.clear()
        listBeans.addAll(datas!!)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holdr {
        return Holdr(LayoutInflater.from(context).inflate(R.layout.progress_layout, parent, false))
    }

    override fun onBindViewHolder(holder: Holdr, position: Int) {
        holder.line.visibility =
            if (position == listBeans.size - 1) View.GONE else View.VISIBLE
        val listBean = listBeans[position]
        holder.time.text = listBean.createTimeStr
        holder.state.text = listBean.str
    }

    override fun getItemCount(): Int {
        return listBeans.size
    }

    inner class Holdr(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val line: View
        val time: TextView
        val state: TextView

        init {
            line = itemView.findViewById(R.id.line)
            time = itemView.findViewById(R.id.tmie)
            state = itemView.findViewById(R.id.state)
        }
    }

    init {
        listBeans = ArrayList()
    }
}