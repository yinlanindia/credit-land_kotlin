package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.EnterRegisterActBinding
import com.app.island.cash.requestview.EnterRegisterReq

class EnterRegister:BaseAct() {
    private lateinit var binding:EnterRegisterActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.enter_register_act)
        val phone = intent.getStringExtra("phone")
        binding.ivFinish.setOnClickListener { finish() }
        phone?.let {
            EnterRegisterReq(this,binding,it)
        }
    }
}