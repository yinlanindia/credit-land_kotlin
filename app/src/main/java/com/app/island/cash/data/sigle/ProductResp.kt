package com.app.island.cash.data.sigle

class ProductResp {
    var realAmount: String? = null
    var onceHandlingFee: String? = null
    var amount: String? = null
    var repaymentAmount: String? = null
    var dayLimit: String? = null
    var repayTime: String? = null
    var interest: String? = null
    var onceServiceFee: String? = null
    var repaymentRemark: String? = null
    var remark: String? = null
    var state: String? = null
    var applicationDate: String? = null
    var gstFee: String? = null
    var borrowId: String? = null
    var bankAccount: String? = null
    var bankName: String? = null
    var riskType: String? = null
}