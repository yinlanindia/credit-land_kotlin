package com.app.island.cash.wegit

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.TextView
import com.app.island.cash.R
import com.app.island.cash.data.sigle.SelectListDataRec

class WebSelectDialog(private val contxt: Context, private val privacy: String) : Dialog(
    contxt, R.style.select_dialog
), View.OnClickListener {
    private var acceptListener: OnListSelectListener? = null
    private var denyListener: OnListSelectListener? = null
    private val data: List<SelectListDataRec.ListItem>? = null
    private val selectedIndex = 0
    private val item: SelectListDataRec.ListItem? = null
    private lateinit var ok: TextView
    private var timer: CountDownTimer? = object : CountDownTimer(5 * 1000, 1000) {
        override fun onTick(l: Long) {
            ok.text = "I have read and agree the privacy policy " + l / 1000 + "s"
        }

        override fun onFinish() {
            ok.text = "I have read and agree the privacy policy"
            ok.isEnabled = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        //填充对话框的布局
        val inflate = LayoutInflater.from(context).inflate(R.layout.webview_list_dialog, null)
        //初始化控件
        inflate.findViewById<View>(R.id.btn_cancel).setOnClickListener(this)
        ok = inflate.findViewById(R.id.btn_ok)
        ok.setOnClickListener(this)
        //将布局设置给Dialog
        setContentView(inflate)
        initDialogAttributes()
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        val webView = inflate.findViewById<WebView>(R.id.webiew)
        val setting = webView.settings
        // 支持缩放
        setting.setSupportZoom(true)
        // 设置支持缩放 + -
        setting.builtInZoomControls = true
        // 关闭 webView 中缓存
        /**/setting.cacheMode = WebSettings.LOAD_NO_CACHE
        setting.useWideViewPort = true
        setting.loadWithOverviewMode = true
        // 设置WebView属性，能够执行Javascript脚本
        setting.javaScriptEnabled = true
        setting.savePassword = false
        setting.domStorageEnabled = true
        setting.defaultTextEncodingName = "utf-8"

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            dealJavascriptLeak(webView);
        }*/if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        webView.loadUrl(privacy)
    }

    private fun initDialogAttributes() {
        //获取当前Activity所在的窗体
        val dialogWindow = window
        //设置Dialog从窗体底部弹出
        dialogWindow!!.setGravity(Gravity.BOTTOM)
        val lp = dialogWindow.attributes
        lp.width = getScreenWidth(contxt as Activity)
        //将属性设置给窗体
        dialogWindow.attributes = lp
    }

    private fun getScreenWidth(activity: Activity): Int {
        val manager = activity.windowManager
        val outMetrics = DisplayMetrics()
        manager.defaultDisplay.getMetrics(outMetrics)
        return outMetrics.widthPixels
    }

    fun setOnAcceptDialogListener(listener: OnListSelectListener?): WebSelectDialog {
        acceptListener = listener
        return this
    }

    fun setOnDenyDialogListener(listener: OnListSelectListener?): WebSelectDialog {
        denyListener = listener
        return this
    }

    override fun show() {
        super.show()
        timer!!.start()
    }

    override fun dismiss() {
        super.dismiss()
        timer!!.cancel()
        timer = null
    }

    private var lastClickTime: Long = 0
    val isCanClick: Boolean
        get() = try {
            val time = System.currentTimeMillis()
            val offSetTime = time - lastClickTime
            if (Math.abs(offSetTime) > 500) {
                lastClickTime = time
                true
            } else {
                false
            }
        } catch (e: Exception) {
            true
        }

    override fun onClick(v: View) {
        if (!isCanClick) {
            return
        }
        val id = v.id
        if (id == R.id.btn_cancel) {
            if (denyListener != null) {
                denyListener!!.onCallBack(this)
            }
            dismiss()
        } else if (id == R.id.btn_ok) {
            if (acceptListener != null) {
                acceptListener!!.onCallBack(this)
            }
            dismiss()
        }
    }

    interface OnListSelectListener {
        fun onCallBack(dialog: WebSelectDialog?)
    }
}