package com.app.island.cash.data.sigle

import com.app.island.cash.utilstools.SystemUtils

class ChangePasswordCall(newPwd: String, oldPwd: String) {
    var newPwd: String
    var oldPwd: String

    init {
        this.newPwd = SystemUtils.md5(newPwd)
        this.oldPwd = SystemUtils.md5(oldPwd)
    }
}