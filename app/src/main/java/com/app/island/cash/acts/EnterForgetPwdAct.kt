package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.EnterForgetPasswordActBinding
import com.app.island.cash.requestview.EnterForgetPwdActReq

class EnterForgetPwdAct:BaseAct() {

    private lateinit var binding:EnterForgetPasswordActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(EnterForgetPwdAct@this, R.layout.enter_forget_password_act)
       val phone = intent.getStringExtra("phone")
        binding.ivFinish.setOnClickListener { finish() }
        phone?.let {
            EnterForgetPwdActReq(this,binding,it )
        }
    }
}