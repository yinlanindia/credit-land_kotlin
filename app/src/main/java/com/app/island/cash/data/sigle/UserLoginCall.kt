package com.app.island.cash.data.sigle

class UserLoginCall(
    private val loginName: String,
    private val loginPwd: String,
    private val imei: String,
    private val mac: String
)