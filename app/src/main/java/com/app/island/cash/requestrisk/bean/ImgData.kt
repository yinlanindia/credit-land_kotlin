package com.app.island.cash.requestrisk.bean

class ImgData {
    @JvmField
    var width //照片宽度
            : String? = null
    @JvmField
    var height //照片高度
            : String? = null
    @JvmField
    var name //文件名
            : String? = null
    @JvmField
    var latitude //拍摄地址纬度
            : String? = null
    @JvmField
    var longitude //拍摄地址经度
            : String? = null
    @JvmField
    var time //拍照日期
            : String? = null
    @JvmField
    var model //相机型号
            : String? = null
    @JvmField
    var make //拍摄者
            : String? = null
}