package com.app.island.cash.acts

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.EnterStartActBinding
import com.app.island.cash.requestview.EnterRequest

class EnterStart:BaseAct() {
    private lateinit var binding: EnterStartActBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(EnterStart@this, R.layout.enter_start_act)
        EnterRequest(EnterStart@this,binding)
    }

    override fun onBackPressed() {

    }
}