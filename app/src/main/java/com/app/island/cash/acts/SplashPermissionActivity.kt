package com.app.island.cash.acts

import android.Manifest
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.island.cash.R
import com.app.island.cash.databinding.ActivitySplashBinding
import com.app.island.cash.data.sigle.UrlListRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.params.AppParams
import com.app.island.cash.utilstools.UserInfoManager
import com.app.island.cash.wegit.PermissionSelectDialog
import com.app.island.cash.wegit.WebSelectDialog
import com.gyf.immersionbar.ImmersionBar
import com.tbruyelle.rxpermissions2.RxPermissions

class SplashPermissionActivity : BaseAct() {
    private var permissionsCheck: RxPermissions? = null
    private var privacyUtl: String? = null
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_splash)
        initIntentParams()
        initData()
    }
     fun initData() {
        UserInfoManager.instance.setCurrent()
        val httpResultCall = NetClient.getService(
            AppService::class.java
        ).cOmmonUrl()
        httpResultCall.enqueue(object : RequestCallBack<HttpResult<UrlListRec>?>() {
            override fun onSuccess(body: HttpResult<UrlListRec>?) {
                val list = body?.data?.list
                list?.let {
                    for (data in it) {
                        if ("h5_protocol_privacy" == data.code) {
                            privacyUtl = AppParams.net_url + data.value
                        }
                    }
                    checkPermissionsSelef()
                }

            }
        })
    }

     fun initIntentParams() {
        ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.white)
            .navigationBarColor(
                R.color.black
            ).statusBarDarkFont(false, 0.2f).init()
    }

    private fun checkPermissionsSelef() {
        permissionsCheck = RxPermissions(this)
        if (permissionsCheck!!.isGranted(Manifest.permission.READ_SMS) && permissionsCheck!!.isGranted(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) && permissionsCheck!!.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
            permissionsCheck!!.isGranted(Manifest.permission.READ_CONTACTS) && permissionsCheck!!.isGranted(
                Manifest.permission.ACCESS_FINE_LOCATION
            ) && permissionsCheck!!.isGranted(Manifest.permission.CAMERA) &&
            permissionsCheck!!.isGranted(Manifest.permission.READ_PHONE_STATE)
        ) {
            binding.root.postDelayed({
                if (!UserInfoManager.instance.isLogin) {
                    startActivity(Intent(this@SplashPermissionActivity, EnterStart::class.java))
                } else {
                    startActivity(Intent(this@SplashPermissionActivity, TabActivity::class.java))
                }
                finish()
            }, 1000)
        } else {
            binding.root.postDelayed({
                PermissionSelectDialog(this@SplashPermissionActivity).setOnAcceptDialogListener(object :
                    PermissionSelectDialog.OnListSelectListener{
                    override fun onCallBack(dialog: PermissionSelectDialog?) {
                        applyPermission(dialog)
                    }

                }).setOnDenyDialogListener(object :PermissionSelectDialog.OnListSelectListener{
                    override fun onCallBack(dialog: PermissionSelectDialog?) {
                        finish()
                    }
                }).show()
            }, 1000)
        }
    }

    private fun applyPermission(dialog: PermissionSelectDialog?) {
        permissionsCheck!!.request(
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE
        ).subscribe { aBoolean ->
            if (aBoolean) {
                dialog?.dismiss()
                privacyUtl?.let {
                    WebSelectDialog(
                        this@SplashPermissionActivity,
                        privacyUtl!!
                    ).setOnDenyDialogListener(object :WebSelectDialog.OnListSelectListener{
                        override fun onCallBack(dialog: WebSelectDialog?) {
                            finish()
                        }
                    }).setOnAcceptDialogListener(object :WebSelectDialog.OnListSelectListener{
                        override fun onCallBack(dialog: WebSelectDialog?) {
                            if (!UserInfoManager.instance.isLogin) {
                                startActivity(
                                    Intent(
                                        this@SplashPermissionActivity,
                                        EnterStart::class.java
                                    )
                                )
                            } else {
                                startActivity(
                                    Intent(
                                        this@SplashPermissionActivity,
                                        TabActivity::class.java
                                    )
                                )
                            }
                            finish()
                        }
                    }).show()
                }

            }
        }
    }
}