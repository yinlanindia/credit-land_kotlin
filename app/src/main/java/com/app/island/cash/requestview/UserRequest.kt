package com.app.island.cash.requestview

import android.content.Intent
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.app.island.cash.acts.Renzhengzhogxin
import com.app.island.cash.acts.HtmlView
import com.app.island.cash.acts.RecordsLoansActivity
import com.app.island.cash.acts.SettinngActivity
import com.app.island.cash.databinding.UserFragBinding
import com.app.island.cash.data.sigle.UrlListRec
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.params.AppParams
import com.app.island.cash.utilstools.NetWorkLoadingUtil

class UserRequest {
    private var context:FragmentActivity
    private var binding: UserFragBinding
    constructor(context:FragmentActivity,binding:UserFragBinding){
        this.context =context
        this.binding = binding
        setOnclcickListener()
    }

    private fun setOnclcickListener() {
        binding.llCenter.setOnClickListener(View.OnClickListener {
            context.startActivity(
                Intent(
                   context,
                    Renzhengzhogxin::class.java
                )
            )
        })
        binding.llSetting.setOnClickListener(View.OnClickListener {
            context.startActivity(
                Intent(
                   context,
                    SettinngActivity::class.java
                )
            )
        })
        binding.llRecord.setOnClickListener(View.OnClickListener {
            context.startActivity(
                Intent(
                   context,
                    RecordsLoansActivity::class.java
                )
            )
        })
        binding.llHelp.setOnClickListener(View.OnClickListener {
            NetWorkLoadingUtil.showDialog(context)
            NetClient.getService(
                AppService::class.java
            ).cOmmonUrl().enqueue(object : RequestCallBack<HttpResult<UrlListRec>?>() {
               override fun onSuccess(body: HttpResult<UrlListRec>?) {
                   val list = body?.data?.list
                   list?.let {
                       for (data in it) {
                           if ("h5_help" == data.code) {
                               val intent = Intent(
                                   context,
                                   HtmlView::class.java
                               )
                               intent.putExtra("title", "Help Center")
                               intent.putExtra("url", AppParams.net_url + data.value)
                               context.startActivity(intent)
                           }
                       }
                   }

                }
            })
        })
    }
}