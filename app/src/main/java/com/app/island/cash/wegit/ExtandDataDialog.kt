package com.app.island.cash.wegit

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.app.island.cash.R

class ExtandDataDialog(
    context: Context,
    private val date: String,
    private val amount: String,
    private val fee: String,
    private val onClickListener: View.OnClickListener
) : Dialog(context, R.style.loading_dialog) {
    private val downtime = 0
    private lateinit var feeTv: TextView
    private lateinit var dataTv: TextView
    private lateinit var amountTV: TextView
    private lateinit var repay: TextView
    private lateinit var cancel: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.extend_date_load)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        dataTv = findViewById(R.id.extend_date)
        feeTv = findViewById(R.id.extend_fee)
        amountTV = findViewById(R.id.extend_amount)
        repay = findViewById(R.id.repay_button)
        cancel = findViewById(R.id.extend_repay)
        // 加载动画
        window!!.attributes.gravity = Gravity.CENTER //居中显示
        window!!.attributes.dimAmount = 0.5f //背景透明度 取值范围 0 ~ 1
        dataTv.setText(date)
        feeTv.setText("₹$fee")
        amountTV.setText("₹$amount")
        repay.setOnClickListener(View.OnClickListener { view ->
            onClickListener.onClick(view)
            dismiss()
        })
        cancel.setOnClickListener(View.OnClickListener { dismiss() })
    }
}