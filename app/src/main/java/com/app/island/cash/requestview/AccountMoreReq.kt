package com.app.island.cash.requestview

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.island.cash.adapter.AddBankAdapter
import com.app.island.cash.databinding.AccountMoreActivityBinding
import com.app.island.cash.data.sigle.BankAccountId
import com.app.island.cash.data.sigle.BankListBack
import com.app.island.cash.requestwork.AppService
import com.app.island.cash.requestwork.HttpResult
import com.app.island.cash.requestwork.NetClient
import com.app.island.cash.requestwork.RequestCallBack
import com.app.island.cash.utilstools.MaterialDialogUtils
import com.app.island.cash.utilstools.NetWorkLoadingUtil

class AccountMoreReq {
    private val context:AppCompatActivity
    private val binding:AccountMoreActivityBinding
    private val adapter: AddBankAdapter
    constructor(context:AppCompatActivity,binding: AccountMoreActivityBinding){
        this.context = context
        this.binding = binding
        binding.tvPageTitle.text="Select bank account"
        binding.ivFinish.setOnClickListener { context.finish() }
        adapter= AddBankAdapter(context,object :AddBankAdapter.SetBankListener{
            override fun setBank(item: BankListBack.Bean) {
                MaterialDialogUtils.showBasicDialogNoTitle(
                    context, "Are you sure to set this card as the default payment card?"
                , View.OnClickListener { shezhimorenyinhanka(item.id!!) }).show()
            }

        })
        binding.banks.adapter = adapter
        binding.banks.layoutManager = LinearLayoutManager(context)
        binding.ivFinish.setOnClickListener {
            context.finish()
        }
        huoquliebiao()
    }

    private fun shezhimorenyinhanka(id: String) {
        NetWorkLoadingUtil.showDialog(context)
        NetClient.getService(
            AppService::class.java
        ).setDefault(BankAccountId(id)).enqueue(object : RequestCallBack<HttpResult<Any>?>() {
            override fun onSuccess(body: HttpResult<Any>?) {
                huoquliebiao()
            }
        })
    }

     fun huoquliebiao() {
         NetWorkLoadingUtil.showDialog(context)
         NetClient.getService(
             AppService::class.java
         ).cardList().enqueue(object : RequestCallBack<HttpResult<BankListBack>?>() {
             override fun onSuccess(body: HttpResult<BankListBack>?) {
                 adapter.setNewData(body?.data?.list)
             }
         })
    }
}