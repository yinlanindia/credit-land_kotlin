package com.app.island.cash.requestrisk.bean

/**
 * Date: 2020-03-10 16:13
 * Author: kay lau
 * Description:
 */
class PermissionCheckInfo {
    var isAccessPermission = false
    var permissionName: String? = null
}