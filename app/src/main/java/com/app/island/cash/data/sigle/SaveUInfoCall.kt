package com.app.island.cash.data.sigle

class SaveUInfoCall(
    var email: String,
    var middleName: String,
    var firstName: String,
    var lastName: String,
    var marital: String,
    var educationalQualification: String,
    var accommodationType: String,
    var childrenNumber: String,
    var loanPurpose: String,
    var occupation: String,
    var salaryRange: String,
    var salarySource: String
)