package com.app.island.cash.data.sigle

import java.util.*

class MarketRec {
    var auth: AuthBean? = null
    var productList: List<ProductListBean>? = null
        get() = if (field == null) ArrayList() else field

    class AuthBean {
        var result = 0
        var qualified = 0
        var total = 0
    }

    class ProductListBean {
        var id = 0
        var productName: String? = null
        var borrowId: String? = null
        var productLogo: String? = null
        var productAmount: String? = null
        var productStatus = 0
        var productInterest: String? = null
        var merchantId = 0
        var isNeedCatch = false
    }
}